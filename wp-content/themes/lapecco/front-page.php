<?php /* Template name: Página Inicial */ get_header(); ?>

<section class="banner">
	<div class="box-nav-banner">
	<?php
		$ctrlB = 0;
		while ( have_rows('banners') ) : the_row();
			$imagem_a_esquerda = get_sub_field('imagem_a_esquerda');
      		$img_srcset_a2 = wp_get_attachment_image_srcset($imagem_a_esquerda['ID'], 'full' );
    ?>
		<div class="img-esq ctrlB<?php echo $ctrlB; ?> <?php if($ctrlB==0){ echo active; } ?>"></div>

	<?php $ctrlB++; endwhile; ?>
		<ul class="nav-banner">
			<?php
				$ctrlB = 0;
				while ( have_rows('banners') ) : the_row();
					$nome_do_titulo = get_sub_field('nome_do_titulo');
					$link_do_botao_para_a_pagina = get_sub_field('link_do_botao_para_a_pagina');
					$icone = get_sub_field('icone');
			?>
					<li>
						<a href="<?php echo $link_do_botao_para_a_pagina; ?>" class="<?php if($ctrlB==0){ echo active; } ?>" data-id="ctrlB<?php echo $ctrlB; ?>">
							<img src="<?php echo $icone['url'] ?>" alt="<?php echo $icone['alt'] ?>"> <?php echo $nome_do_titulo; ?>
						</a>
					</li>

			<?php $ctrlB++; endwhile; ?>
		</ul>
	</div>
	<?php
		$ctrlB = 0;
		while ( have_rows('banners') ) : the_row();
			$nome_do_titulo = get_sub_field('nome_do_titulo');
			$subtitulo = get_sub_field('subtitulo');
			$link_do_botao_para_a_pagina = get_sub_field('link_do_botao_para_a_pagina');
			$nome_do_botao = get_sub_field('nome_do_botao');

			$imagem_a_direita = get_sub_field('imagem_a_direita');
      		$img_srcset_a = wp_get_attachment_image_srcset($imagem_a_direita['ID'], 'full' );
    ?>
			<div class="box-cont <?php if($ctrlB==0){ echo active; } ?>" id="ctrlB<?php echo $ctrlB; ?>">
				<img src="<?php echo $imagem_a_direita['url'];?>" alt="<?php echo $imagem_a_direita['alt'];?>" >
				<div class="bx-rig">
					<h2><?php echo $nome_do_titulo; ?></h2>
					<h3><?php echo $subtitulo; ?></h3>
					<div class="btn gradient">
						<a href="<?php echo $link_do_botao_para_a_pagina; ?>"><span><?php echo $nome_do_botao; ?></span></a>
					</div>
				</div>
			</div>
		<?php $ctrlB++; endwhile; ?>
</section>

<section class="sobre wow fadeIn">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="title-small"><span><?php echo get_field('titulo_sobre'); ?></span></h2>
				<h3 class="title"><?php echo get_field('subtitulo_sobre'); ?></h3>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="bx-gray">
					<h4><?php echo get_field('titulo_box_esquerda'); ?></h4>
					<p><?php echo get_field('conteudo_box_esquerda'); ?></p>
				</div>
				<!-- <div class="bx-white">
					<?php
						$imgSelo = get_field('imagem_selo');
						$img_srcset_a2 = wp_get_attachment_image_srcset($imgSelo['ID'], 'full' );
					?>

					<img src="<?php echo $imgSelo['url'];?>" srcset="<?php echo esc_attr( $img_srcset_a2 ); ?>" sizes="(max-width: 80em) 100vw, 98px" alt="<?php echo $imgSelo['alt'];?>">


					<h4><?php echo get_field('titulo_selo'); ?></h4>
					<p><?php echo get_field('conteudo_selo'); ?></p>
				</div> -->
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="convenios">
					<h4><?php echo get_field('titulo_box_direito'); ?></h4>
					<p><?php echo get_field('conteudo_box_direito'); ?></p>
					<form class="cons-conven">
						<div class="campo">
						    <select name="tipo-servicos2" id="tipoServicos2" aria-required="true" aria-invalid="false">
						    	<option value="Selecione o tipo de Serviço">Selecione o tipo de Serviço</option>
						    	<option value="Odontologia">Odontologia</option>
						    	<option value="Especialidades Médicas">Especialidades Médicas</option>
						    	<option value="Exames de imagem">Exames de imagem</option>
						    	<option value="Exames Laboratoriais">Exames Laboratoriais</option>
						    </select>
						</div>
						<div class="campo">
						    <input type="text" id="completeConve" placeholder="Selecione o tipo de Serviço primeiro" disabled="">
						    <svg class="svgLoad" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-spin"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><g transform="translate(50 50)"><g transform="rotate(0) translate(34 0)"><circle cx="0" cy="0" r="8" fill="#000"><animate attributeName="opacity" from="1" to="0.1" begin="0s" dur="1s" repeatCount="indefinite"></animate><animateTransform attributeName="transform" type="scale" from="1.5" to="1" begin="0s" dur="1s" repeatCount="indefinite"></animateTransform></circle></g><g transform="rotate(45) translate(34 0)"><circle cx="0" cy="0" r="8" fill="#000"><animate attributeName="opacity" from="1" to="0.1" begin="0.12s" dur="1s" repeatCount="indefinite"></animate><animateTransform attributeName="transform" type="scale" from="1.5" to="1" begin="0.12s" dur="1s" repeatCount="indefinite"></animateTransform></circle></g><g transform="rotate(90) translate(34 0)"><circle cx="0" cy="0" r="8" fill="#000"><animate attributeName="opacity" from="1" to="0.1" begin="0.25s" dur="1s" repeatCount="indefinite"></animate><animateTransform attributeName="transform" type="scale" from="1.5" to="1" begin="0.25s" dur="1s" repeatCount="indefinite"></animateTransform></circle></g><g transform="rotate(135) translate(34 0)"><circle cx="0" cy="0" r="8" fill="#000"><animate attributeName="opacity" from="1" to="0.1" begin="0.37s" dur="1s" repeatCount="indefinite"></animate><animateTransform attributeName="transform" type="scale" from="1.5" to="1" begin="0.37s" dur="1s" repeatCount="indefinite"></animateTransform></circle></g><g transform="rotate(180) translate(34 0)"><circle cx="0" cy="0" r="8" fill="#000"><animate attributeName="opacity" from="1" to="0.1" begin="0.5s" dur="1s" repeatCount="indefinite"></animate><animateTransform attributeName="transform" type="scale" from="1.5" to="1" begin="0.5s" dur="1s" repeatCount="indefinite"></animateTransform></circle></g><g transform="rotate(225) translate(34 0)"><circle cx="0" cy="0" r="8" fill="#000"><animate attributeName="opacity" from="1" to="0.1" begin="0.62s" dur="1s" repeatCount="indefinite"></animate><animateTransform attributeName="transform" type="scale" from="1.5" to="1" begin="0.62s" dur="1s" repeatCount="indefinite"></animateTransform></circle></g><g transform="rotate(270) translate(34 0)"><circle cx="0" cy="0" r="8" fill="#000"><animate attributeName="opacity" from="1" to="0.1" begin="0.75s" dur="1s" repeatCount="indefinite"></animate><animateTransform attributeName="transform" type="scale" from="1.5" to="1" begin="0.75s" dur="1s" repeatCount="indefinite"></animateTransform></circle></g><g transform="rotate(315) translate(34 0)"><circle cx="0" cy="0" r="8" fill="#000"><animate attributeName="opacity" from="1" to="0.1" begin="0.87s" dur="1s" repeatCount="indefinite"></animate><animateTransform attributeName="transform" type="scale" from="1.5" to="1" begin="0.87s" dur="1s" repeatCount="indefinite"></animateTransform></circle></g></g></svg>
						</div>
						<span id="msgAuto"></span>
					</form>

					<div class="btn gradiente center">
						<a href="<?php echo get_field('link_convenios'); ?>"><span>Ver todos os convênios</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="odon slide">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="title-small blue"><span><?php echo get_field('titulo_odontologia'); ?></span></h2>
				<h3 class="title"><?php echo get_field('subtitulo_odontologia'); ?></h3>
			</div>
			<div class="col-xs-12">
				<div class="owl-carousel">
					<?php
						$countDelay = 0.0;
						$argsOdont = array(
					        'post_type'				=> 'odontologia',
					        'posts_per_page'	=> -1
					    );
					    $odont = new WP_Query( $argsOdont );

					    if($odont->have_posts()) : while( $odont->have_posts() ) {
					        $odont->the_post();
					        $imagem_da_chamada_do_servico = get_field('imagem_da_chamada_do_servico');
					?>
						<div class="item wow fadeInUp" data-wow-duration="1s" data-wow-delay="<?php echo $countDelay; ?>s">
							<a href="<?php the_permalink(); ?>"></a>
							<div class="bx-img">
								<img src="<?php echo $imagem_da_chamada_do_servico['url'] ?>" alt="<?php echo $imagem_da_chamada_do_servico['alt'] ?>" title="<?php echo $imagem_da_chamada_do_servico['title'] ?>">
							</div>
							<span><?php the_title(); ?></span>
						</div>
					<?php $countDelay = $countDelay + 0.2; } endif;
						wp_reset_query();
					?>
				</div>

				<div class="btn gradient center">
					<a href="<?php echo get_field('link_odontologia'); ?>"><span>Conheça as especialidades</span></a>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="exa-lab slide">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="title-small blue"><span><?php echo get_field('titulo_laboratoriais'); ?></span></h2>
				<h3 class="title"><?php echo get_field('subtitulo_laboratoriais'); ?></h3>
			</div>
			<div class="col-xs-12">
				<div class="owl-carousel">
					<?php
						$countDelay = 0.0;

					    while(have_rows('carrousel_laboratoriais')) {
					        the_row();
					        $imagem = get_sub_field('imagem');
					        $nome = get_sub_field('nome');
					        $link = get_sub_field('link');
					?>
						<div class="item wow fadeInUp" data-wow-duration="1s" data-wow-delay="<?php echo $countDelay; ?>s">
							<a href="<?php echo $link; ?>"></a>
							<div class="bx-img">
								<img src="<?php echo $imagem['url']; ?>" alt="<?php echo $imagem['alt']; ?>" title="<?php echo $imagem['title']; ?>">
							</div>
							<span><?php echo $nome; ?></span>
						</div>
					<?php $countDelay = $countDelay + 0.2; } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include(TEMPLATEPATH . '/template-parts/slide-consultas.php');  ?>


<section class="exa-img slide">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="title-small blue"><span><?php echo get_field('titulo_exames_de_imagem'); ?></span></h2>
				<h3 class="title"><?php echo get_field('subtitulo_exames_de_imagem'); ?></h3>
			</div>
			<div class="col-xs-12">
				<div class="owl-carousel">
					<?php
						$countDelay = 0.0;
						$argsOdont = array(
					        'post_type'				=> 'exames-de-imagem',
					        'posts_per_page'	=> -1
					    );
					    $odont = new WP_Query( $argsOdont );

					    if($odont->have_posts()) : while( $odont->have_posts() ) {
					        $odont->the_post();
					        $imagem_da_chamada_do_servico = get_field('imagem_da_chamada_do_servico');
					?>
						<div class="item wow fadeInUp" data-wow-duration="1s" data-wow-delay="<?php echo $countDelay; ?>s">
							<a href="<?php the_permalink(); ?>"></a>
							<div class="bx-img">
								<img src="<?php echo $imagem_da_chamada_do_servico['url'] ?>" alt="<?php echo $imagem_da_chamada_do_servico['alt'] ?>" title="<?php echo $imagem_da_chamada_do_servico['title'] ?>">
							</div>
							<span><?php the_title(); ?></span>
						</div>
					<?php $countDelay = $countDelay + 0.2; } endif;
						wp_reset_query();
					?>
				</div>

				<div class="btn gradient center">
					<a href="<?php echo get_field('link_exames_de_imagem'); ?>"><span>Conheça os exames</span></a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>