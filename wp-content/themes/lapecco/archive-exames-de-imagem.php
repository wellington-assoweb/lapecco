<?php get_header(); ?>

<section class="head-title" style="background-image:url('<?php echo THEMEURL ?>/assets/img/header-default.jpg')">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-name">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                    } ?>
                    <h1 class="title-princ"><?php post_type_archive_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="servicos">
    <div class="my-container menor">
        <div class="row">
            <?php
                $argsEspec = array(
                    'post_type'             => 'exames-de-imagem',
                    'posts_per_page'    => -1,
                    'orderby' => 'title',
                    'order'   => 'ASC'
                );
                $espec = new WP_Query( $argsEspec );

                if($espec->have_posts()) : while( $espec->have_posts() ) {
                    $espec->the_post();
                    $icone_chamada_do_servico = get_field('icone_chamada_do_servico');
            ?>
                    <div class="item">
                        <a href="<?php the_permalink(); ?>">
                            <img src="<?php echo $icone_chamada_do_servico['url'] ?>" alt="<?php echo $icone_chamada_do_servico['alt'] ?>">
                        </a>
                        <h3><?php echo the_title(); ?></h3>
                        <div class="btn gradient center">
                            <a href="<?php the_permalink(); ?>"><span>Saiba Mais</span></a>
                        </div>
                    </div>
            <?php }; endif; wp_reset_query(); ?>
        </div>
    </div>
</section>


<?php
    $argsServPage = array(
        'post__in' => array( 419 ),
        'post_type' => 'page'
    );
    $queryServPage = new WP_Query( $argsServPage );

    while( $queryServPage->have_posts() ) {
        $queryServPage->the_post();
        if(have_rows('diferencial_exames_de_imagem')){
?>
        <section class="dif">
            <div class="my-container menor">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="title"><?php echo get_field('titulo_diferencial_exames_de_imagem'); ?></h2>
                        <div class="owl-carousel">
                        <?php
                            while ( have_rows('diferencial_exames_de_imagem') ) : the_row();
                                $icone_diferencial_exames_de_imagem = get_sub_field('icone_diferencial_exames_de_imagem');
                                $conteudo_diferencial_exames_de_imagem = get_sub_field('conteudo_diferencial_exames_de_imagem');
                        ?>
                                <div class="item">
                                    <img src="<?php echo $icone_diferencial_exames_de_imagem['url'] ?>" alt="<?php echo $icone_diferencial_exames_de_imagem['alt'] ?>">
                                    <?php echo $conteudo_diferencial_exames_de_imagem; ?>
                                </div>
                        <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<?php }; }; wp_reset_query(); ?>

<?php include(TEMPLATEPATH . '/template-parts/melhores-prof.php'); ?>
<?php get_footer(); ?>