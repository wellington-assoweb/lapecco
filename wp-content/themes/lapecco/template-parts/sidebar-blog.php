
<?php if ( $paged >= 2 ){ ?>

    <style>
        #header-sroll .over{
            box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15);
            transition-delay: .1s;
            height: 100%;
            opacity: .99;
        }
    </style>

<?php } ?>

<div class="sidebar">
	<div class="item">
		<div class="btn gradient center">
		    <a class="assinar" href="#">
		        <span>Assine nossas Notícias</span>
		    </a>
		</div>
	</div>

	<div class="item">
		<h3 class="title-side">Categorias</h3>
		<ul class="categorias-sidebar">
			<?php
			$args = array(
				'orderby' => 'name',
				'parent' => 0
		  	);
			$categories = get_categories( $args );
			$j = 1;
			foreach ( $categories as $category ) {
			    echo '<li><a href="' . get_category_link( $category->term_id ) . '"><span>' . $category->name . '</span></a></li>';
			    if ($j == 6)break;
			    $j++;
			}
			?>
		</ul>
	</div>
	<div class="item">
		<h3 class="title-side">Acompanhe nas redes</h3>
		<div class="social">
			<?php
			   	$home = array('post_type' => 'page', 'page_id' => 2 ); // 2 = front-page
			    $home = new WP_Query( $home );
			    while ( $home->have_posts() ) : $home->the_post();
			    	if (get_field('facebook'))
			    		echo '<a href="'.get_field('facebook').'" target="_blank"><i class="icon-facebook-squared"></i></a>';
			    	if (get_field('twitter'))
			    		echo '<a href="'.get_field('twitter').'" target="_blank"><i class="icon-twitter"></i></a>';
			    	if (get_field('youtube'))
			    		echo '<a href="'.get_field('youtube').'" target="_blank"><i class="icon-youtube-play"></i></a>';
			    	if (get_field('instagram'))
			    		echo '<a href="'.get_field('instagram').'" target="_blank"><i class="icon-instagram"></i></a>';
			    	if (get_field('google_plus'))
			    		echo '<a href="'.get_field('google_plus').'" target="_blank"><i class="icon-gplus"></i></a>';
				endwhile;
    			wp_reset_query();
    		?>
		</div>
	</div>

	<div class="item">
		<div class="btn gradient center">
			<a href="<?php echo SITEURL ?>/a-empresa">
				<span>Conheça a Lapecco</span>
			</a>
		</div>
	</div>
</div>



<div id="modalPersNews">
	<div class="mask-news"></div>
	<div class="container-modal">
		<div class="newsletter">
			<div class="cd-form floating-labels">
				<svg version="1.1" id="closeModal" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 23.331 23.331" style="enable-background:new 0 0 23.331 23.331;" xml:space="preserve">
					<path style="fill:#fff;" d="M16.043,11.667L22.609,5.1c0.963-0.963,0.963-2.539,0-3.502l-0.875-0.875
					c-0.963-0.964-2.539-0.964-3.502,0L11.666,7.29L5.099,0.723c-0.962-0.963-2.538-0.963-3.501,0L0.722,1.598
					c-0.962,0.963-0.962,2.539,0,3.502l6.566,6.566l-6.566,6.567c-0.962,0.963-0.962,2.539,0,3.501l0.876,0.875
					c0.963,0.963,2.539,0.963,3.501,0l6.567-6.565l6.566,6.565c0.963,0.963,2.539,0.963,3.502,0l0.875-0.875
					c0.963-0.963,0.963-2.539,0-3.501L16.043,11.667z"/>
				</svg>
				<h3>Mande-nos um email, e fique por dentro de todas as novidades.</h3>
				<div class="fomr-newsletter">
					<?php echo do_shortcode('[contact-form-7 id="751" title="Newsletter"]'); ?>
				</div>
			</div>
		</div>
	</div>
</div>