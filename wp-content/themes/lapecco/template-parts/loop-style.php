<?php

	echo '<style>';
		if (is_front_page()):
				$ctrlB = 0;
				while ( have_rows('banners') ) : the_row();
					$imagem_a_esquerda = get_sub_field('imagem_a_esquerda');

					echo '.ctrlB'.$ctrlB.'{background-image:url('.$imagem_a_esquerda["url"].')}';

					$ctrlB++;
				endwhile;
				$blogHome = array(
					'posts_per_page' => 3,
	                'ignore_sticky_posts' => true,              // Ignora posts fixos
	                'orderby'             => 'meta_value_num',  // Ordena pelo valor da post meta
	                'meta_key'            => 'tp_post_counter', // A nossa post meta
	                'order'               => 'DESC'             // Ordem decrescente
	            );
                $postsHome = new WP_Query( $blogHome );
                if($postsHome->have_posts()) :
                    while( $postsHome->have_posts() ):
                        $postsHome->the_post();
                    	$img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' );
		                if ($img_post_art[0]):
		                	echo '.post-home-'.$post->ID.'{background-image:url('.$img_post_art[0].')}';
		                else:
		                    echo '.post-home-'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default-blog.jpg)}';
		                endif;
                    endwhile;
                endif;
                wp_reset_query();
		elseif(is_page(392)):
				while ( have_rows('imagens_sessão_slide') ) : the_row();
					$imagem = get_sub_field('imagem');
					echo '.slide-emp-'.$imagem["id"].'{background-image:url('.$imagem["url"].')}';
				endwhile;
		elseif(is_page()):
			$img_title = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			if ($img_title[0]):
            	echo '.bg-header-'.$post->ID.'{background-image:url('.$img_title[0].') !important}';
            else:
                echo '.bg-header-'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/header-default.jpg) !important}';
            endif;

		elseif(is_home()):
			$img_title = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			echo '.bg-header-home-blog{background-image:url('.THEMEURL.'/assets/img/bg-header-blog.jpg) !important}';


			$id_dest_exib = 0;
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            if ( $paged < 2 ):
                $args = array(
                    'posts_per_page'      => 1,
                    'post__in'            => get_option( 'sticky_posts' ),
                    'ignore_sticky_posts' => 1
                );
                $destaque_query = new WP_Query( $args );


                if($destaque_query->have_posts()) : while( $destaque_query->have_posts() ):
	                    $destaque_query->the_post();
	                    $id_dest_exib = $post->ID;


	                    $img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' );
		                if ($img_post_art[0]):
		                	echo '.bg-destak-'.$post->ID.'{background-image:url('.$img_post_art[0].')}';
		                else:
		                    echo '.bg-destak-'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default-blog.jpg)}';
		                endif;
	                endwhile;
                endif;
				wp_reset_query();
            endif;

            $sticky = get_option( 'sticky_posts' );
            $argsUlt = array(
                'posts_per_page' => 6,
                'ignore_sticky_posts' => true,              // Ignora posts fixos
                'paged'          => $paged,
                'post__not_in' => $sticky
            );
            $ultimos_posts = new WP_Query( $argsUlt );

            if($ultimos_posts->have_posts()) :
                while( $ultimos_posts->have_posts() ):
                    $ultimos_posts->the_post();

					$img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' );
	                if ($img_post_art[0]):
	                	echo '.post-ult-'.$post->ID.'{background-image:url('.$img_post_art[0].')}';
	                else:
	                    echo '.post-ult-'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default-blog.jpg)}';
	                endif;
                endwhile;
            endif;
            wp_reset_query();

		elseif(is_category() || is_single() || is_author() || is_search()):
			if(is_singular('especialidade-medica') || is_singular('odontologia') || is_singular('exames-laboratoriais') || is_singular('exames-de-imagem') || is_singular('concursos')){
					$img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' );
	                if ($img_post_art[0]):
	                	echo '.bg-header-'.$post->ID.'{background-image:url('.$img_post_art[0].') !important}';
	                else:
	                    echo '.bg-header-'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/bg-header-default-single.jpg) !important}';
	                endif;
					while ( have_rows('slides') ) : the_row();
                    	$imagem2 = get_sub_field('imagem');
                    	echo '.slidebg-'.$imagem2['id'].'{background-image:url("'.$imagem2['url'].'")}';
                    endwhile;

					$argsServPage = array(
				        'post__in' => array( 419 ),
				        'post_type' => 'page'
				    );
				    $queryServPage = new WP_Query( $argsServPage );

				    $queryServPage->the_post();

				    if(is_singular('especialidade-medica')){
					    $fundo_vantagens_especialidade_medica = get_field('fundo_vantagens_especialidade_medica');
					    $fundo_slide_especialidade_medica = get_field('fundo_slide_especialidade_medica');

					    echo '.bg-vant-'.$fundo_vantagens_especialidade_medica["ID"].'{background-image:url('.$fundo_vantagens_especialidade_medica["url"].')}';
					    echo '.bg-slide-'.$fundo_slide_especialidade_medica["ID"].'{background-image:url('.$fundo_slide_especialidade_medica["url"].')}';

				    }elseif(is_singular('odontologia')){
				    	$fundo_vantagens_odontologia = get_field('fundo_vantagens_odontologia');
					    $fundo_slide_odontologia = get_field('fundo_slide_odontologia');

					    echo '.bg-vant-'.$fundo_vantagens_odontologia["ID"].'{background-image:url('.$fundo_vantagens_odontologia["url"].')}';
					    echo '.bg-slide-'.$fundo_slide_odontologia["ID"].'{background-image:url('.$fundo_slide_odontologia["url"].')}';

				    }elseif(is_singular('exames-laboratoriais')){
				    	$fundo_vantagens_exames_laboratoriais = get_field('fundo_vantagens_exames_laboratoriais');
					    $fundo_slide_exames_laboratoriais = get_field('fundo_slide_exames_laboratoriais');

					    echo '.bg-vant-'.$fundo_vantagens_exames_laboratoriais["ID"].'{background-image:url('.$fundo_vantagens_exames_laboratoriais["url"].')}';
					    echo '.bg-slide-'.$fundo_slide_exames_laboratoriais["ID"].'{background-image:url('.$fundo_slide_exames_laboratoriais["url"].')}';

				    }elseif(is_singular('exames-de-imagem')){
				    	$fundo_vantagens_exames_de_imagem = get_field('fundo_vantagens_exames_de_imagem');
					    $fundo_slide_exames_de_imagem = get_field('fundo_slide_exames_de_imagem');

					    echo '.bg-vant-'.$fundo_vantagens_exames_de_imagem["ID"].'{background-image:url('.$fundo_vantagens_exames_de_imagem["url"].')}';
					    echo '.bg-slide-'.$fundo_slide_exames_de_imagem["ID"].'{background-image:url('.$fundo_slide_exames_de_imagem["url"].')}';
				    }
				    elseif(is_singular('concursos')){
				    	$fundo_vantagens_concurso = get_field('fundo_vantagens_concurso');
					    $fundo_slide_concurso = get_field('fundo_slide_concurso');

					    echo '.bg-vant-'.$fundo_vantagens_concurso["ID"].'{background-image:url('.$fundo_vantagens_concurso["url"].')}';
					    echo '.bg-slide-'.$fundo_slide_concurso["ID"].'{background-image:url('.$fundo_slide_concurso["url"].')}';
				    }
					wp_reset_query();
			}elseif(is_search() || is_category()){
				echo '.bg-header-home-blog{background-image:url('.THEMEURL.'/assets/img/bg-header-blog.jpg) !important}';

				if (have_posts()):
					while(have_posts()):
						the_post();

						$img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' );
		                if ($img_post_art[0]):
		                	echo '.post-ult-'.$post->ID.'{background-image:url('.$img_post_art[0].')}';
		                else:
		                    echo '.post-ult-'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default-blog.jpg)}';
		                endif;

					endwhile;
				else:
					$sticky = get_option( 'sticky_posts' );
					$argsSearch = array(
						'post_type'      => 'post',
						'posts_per_page' => 2,
						'orderby'        => 'rand',
						'ignore_sticky_posts' => true,              // Ignora posts fixos
						'post__not_in' => $sticky
					);
					$postsSearch = new WP_Query( $argsSearch );
	        		while ( $postsSearch->have_posts() ) : $postsSearch->the_post();

	        			$img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' );
		                if ($img_post_art[0]):
		                	echo '.post-ult-'.$post->ID.'{background-image:url('.$img_post_art[0].')}';
		                else:
		                    echo '.post-ult-'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default-blog.jpg)}';
		                endif;

	        		endwhile;
	        		session_start();
			        $_SESSION["postsSearch"] = $postsSearch;
			    endif;


			}else{
				echo '.bg-header-home-blog{background-image:url('.THEMEURL.'/assets/img/bg-header-blog.jpg) !important}';

				$img_title = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				if ($img_title[0]):
	            	echo '.bg-destak-'.$post->ID.'{background-image:url('.$img_title[0].') !important}';
	            endif;



                $idCategory = get_the_category();
                $idCategory = $idCategory[0]->term_id;
                $argsRelacSingle = array(
                    'posts_per_page' => 2,
                    'post__not_in'   => array($post->ID),
                    'cat' => $idCategory,
                    'orderby'   => 'rand'
                );
                $postRelacSingle = new WP_Query( $argsRelacSingle );



                if($postRelacSingle->have_posts()) :
                    while( $postRelacSingle->have_posts() ):
                        $postRelacSingle->the_post();

                    	$img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' );
		                if ($img_post_art[0]):
		                	echo '.post-relac-'.$post->ID.'{background-image:url('.$img_post_art[0].')}';
		                else:
		                    echo '.post-relac-'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default-blog.jpg)}';
		                endif;
                     endwhile;


                else:
                    wp_reset_query();

                    $argsUlt = array(
                        'posts_per_page' => 2,
                        'paged'          => $paged,
                        'post__not_in'   => array($post->ID)
                    );

                    $ultimos_posts = new WP_Query( $argsUlt );

                    while( $ultimos_posts->have_posts() ):
                        $ultimos_posts->the_post();

						$img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' );
		                if ($img_post_art[0]):
		                	echo '.post-relac-'.$post->ID.'{background-image:url('.$img_post_art[0].')}';
		                else:
		                    echo '.post-relac-'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default-blog.jpg)}';
		                endif;

					endwhile;
                endif;
                wp_reset_query();

                session_start();
		        $_SESSION["postRelacSingle"] = $postRelacSingle;

			}
		endif;
		if(is_category()):
			if(have_posts()) : while( have_posts() ) {
                the_post();

				$img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' );
                if ($img_post_art[0]):
                	echo '.bg-img-res-'.$post->ID.'{background-image:url('.$img_post_art[0].')}';
                else:
                    echo '.bg-img-res-'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default-blog.jpg)}';
                endif;
            }
            endif;
		endif;

        wp_reset_query();
	echo '</style>';
?>