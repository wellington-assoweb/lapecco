 <?php
    $argsServPage = array(
        'post__in' => array( 488 ),
        'post_type' => 'page'
    );
    $queryServPage = new WP_Query( $argsServPage );

    if($queryServPage->have_posts()) : while( $queryServPage->have_posts() ) {
        $queryServPage->the_post();
?>
<section class="melhores-prof">
    <div class="my-container menor">
        <div class="row">
            <div class="col-xs-12">

                <img class="carimbo" src="<?php echo THEMEURL ?>/assets/img/carimbo-lapecco.png" alt="Carimbo de qualidade Lapecco" title="Carimbo de qualidade Lapecco">

                <div class="quadro">
                    <img class="bg" src="<?php echo THEMEURL ?>/assets/img/quadro-lapecco.jpg" alt="Quadro em branco com médico segurando" title="Quadro em branco">
                    <img class="qualid" src="<?php echo THEMEURL ?>/assets/img/carimbo-de-qualidade-lapecco.png" alt="Qualidade Lapecco" title="Qualidade Lapecco">

                    <div class="bx-cont">
                        <h2 class="title-custom"><?php echo get_field('titulo_sessao_carimbo'); ?></h2>
                        <?php echo get_field('conteudo_sessao_carimbo'); ?>
                        <div class="btn gradient center">
                            <a href="<?php echo get_field('link_do_botao_carimbo'); ?>">
                                <span><?php echo get_field('texto_do_botao_carimbo'); ?></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php }; endif; wp_reset_query(); ?>