<?php
$img_title = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$url_img_title = $img_title['0']; ?>
<?php if (is_home() || is_category() || is_single() || is_author() || is_search()){ ?>
	<section class="head-title" style="background-image:url('<?php echo THEMEURL ?>/assets/img/blog.jpg')">
<?php }else if ($img_title){ ?>
	<section class="head-title" style="background-image:url('<?php echo $url_img_title ?>')">
<?php }else{ ?>
	<section class="head-title" style="background-image:url('<?php echo THEMEURL; ?>/assets/img/default.jpg')">
<?php } ?>
	<div class="big-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-name">
					<?php if (is_404()){ ?>
						<h1 class="title-princ"><strong>404 - PÁGINA NÃO ENCONTRADA</strong></h1>
					<?php }elseif(is_search()){ ?>
						<h1 class="title-princ">Blog Busca</h1>
					<?php }elseif(is_page()){ ?>
						<h1 class="title-princ"><?php the_title(); ?></h1>
					<?php }elseif(is_home()){ ?>
						<h1 class="title-princ">Notícias</h1>
					<?php }elseif(is_page() || is_single()){ ?>
						<h1 class="title-princ"><?php the_title(); ?></h1>
					<?php }elseif(is_category()){ ?>
						<h1 class="title-princ"><?php printf( __( ' %s', '' ), '' . single_cat_title( '', false ) . '' ); ?></h1>
					<?php }elseif(is_author()){ ?>
						<h1 class="title-princ">Artigos de <?php the_author(); ?></h1>
					<?php }elseif(is_post_type_archive('case')){ ?>
						<h1 class="title-princ">Cases</h1>
					<?php } ?>
					<?php if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
					} ?>
				</div>
			</div>
		</div>
	</div>
</section>