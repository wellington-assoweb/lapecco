<section class="cons slide">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<?php if(is_front_page()){ ?>
					<h2 class="title-small blue"><span><?php echo get_field('titulo_especialidade_medica'); ?></span></h2>
					<h3 class="title"><?php echo get_field('subtitulo_especialidade_medica'); ?></h3>
				<?php }else{ ?>
					<h3 class="title">Consultas médicas em BH com especialistas</h3>
				<?php } ?>
			</div>
			<div class="col-xs-12">
				<div class="owl-carousel">
					<?php
						$countDelay = 0.0;
						$argsOdont = array(
					        'post_type'			=> 'especialidade-medica',
					        'posts_per_page'	=> -1,
					        'post__not_in' 		=> array($post->ID)
					    );
					    $odont = new WP_Query( $argsOdont );

					    if($odont->have_posts()) : while( $odont->have_posts() ) {
					        $odont->the_post();
				        	$imagem_da_chamada_do_servico = get_field('imagem_da_chamada_do_servico');
 					?>
							<div class="item wow fadeInUp" data-wow-duration="1s" data-wow-delay="<?php echo $countDelay ?>s">
								<a href="<?php the_permalink(); ?>"></a>
								<div class="bx-img">
									<img src="<?php echo $imagem_da_chamada_do_servico['url'] ?>" alt="">
								</div>
								<span><?php the_title(); ?></span>
							</div>
					<?php
							$countDelay = $countDelay + 0.2;
						}
						endif;
						wp_reset_query();
					?>
				</div>
				<div class="btn gradient center">
					<a href="<?php echo SITEURL ?>/especialidade-medica"><span>Conheça as especialidades</span></a>
				</div>
			</div>
		</div>
	</div>
</section>