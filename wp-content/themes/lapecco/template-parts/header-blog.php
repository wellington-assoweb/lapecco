<section class="head-title bg-header-home-blog">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="page-name">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                    } ?>
                    <h2 class="title-princ">Blog</h2>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <?php if(!is_search()): ?>
                    <form class="busca" id="formheader" method="get" action="<?php echo SITEURL; ?>/">
                        <input class="sb-search-input" type="text" value="" name="s" id="s" required>
                        <input class="sb-search-submit" type="submit" value="Buscar">
                        <i class="icon-search"></i>
                    </form>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>