<?php get_header(); /* Template name: Blog */
    include(TEMPLATEPATH . '/template-parts/header-blog.php');
?>




<div class="box-blog <?php if ( $paged >= 2 ){ echo 'nao-destaque'; } ?>" itemscope="" itemtype="http://schema.org/BlogPosting">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
            <?php
                $id_dest_exib = 0;
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                if ( $paged < 2 ):
                    $args = array(
                        'posts_per_page'      => 1,
                        'post__in'            => get_option( 'sticky_posts' ),
                        'ignore_sticky_posts' => 1
                    );
                    $destaque_query = new WP_Query( $args );


                    if($destaque_query->have_posts()) : while( $destaque_query->have_posts() ) {
                        $destaque_query->the_post();
                        $id_dest_exib = $post->ID;
                ?>
                        <article class="blog-destak">
                            <div class="col-xs-12 col-md-7">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="bx-img bg-destak-<?php echo $id; ?>"></div>
                                </a>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <div class="bx-texto">
                                    <h2><a href="<?php the_permalink(); ?>"><?php the_title_limit($post->Id, 55); ?></a></h2>
                                    <div class="date">
                                        <data itemprop="datePublished" value="<?php the_time('j \d\e F \d\e Y'); ?>"><?php the_time('j \d\e F \d\e Y'); ?></data>
                                    </div>
                                    <a href="<?php the_permalink(); ?>"><p><?php the_content_limit(300); ?></p></a>
                                </div>
                                <div class="btn gradient center">
                                    <a href="<?php the_permalink(); ?>"><span>Continuar Lendo</span></a>
                                </div>
                            </div>
                        </article>
            <?php   }
                    endif;
                endif;
                wp_reset_query();

            ?>
                <div class="posts-rest">
                    <div class="row">
                        <?php
                        $sticky = get_option( 'sticky_posts' );
                        $argsUlt = array(
                            'posts_per_page' => 6,
                            'ignore_sticky_posts' => true,              // Ignora posts fixos
                            'paged'          => $paged,
                            'post__not_in' => $sticky
                        );
                        $ultimos_posts = new WP_Query( $argsUlt );

                        if($ultimos_posts->have_posts()) :
                            while( $ultimos_posts->have_posts() ):
                                $ultimos_posts->the_post();

                        ?>
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <article class="one-article" itemscope itemtype="http://schema.org/NewsArticle">
                                        <div class="categ">
                                            <?php
                                            $categoria = get_the_category();
                                            foreach($categoria as $category) {
                                                $output = '<a class="nome-categoria" href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "Ver todos os posts de %s" ), $category->name ) ) . '"><span>'.$category->cat_name.'</span></a>';
                                            }
                                            echo $output; ?>
                                        </div>
                                        <a href="<?php the_permalink(); ?>">
                                            <figure itemprop="image" class="post-ult-<?php echo $id; ?>">
                                                <figcaption class="hidden">
                                                    <h4><?php the_title(); ?></h4>
                                                </figcaption>
                                            </figure>
                                        </a>
                                        <div class="description">
                                            <h3 class="title-post" itemprop="headline">
                                                <a href="<?php the_permalink(); ?>"><?php the_title_limit($post->Id, 55); ?></a>
                                            </h3>
                                            <div class="date">
                                                <data itemprop="datePublished" value="<?php the_time('j \d\e F \d\e Y'); ?>"><?php the_time('j \d\e F \d\e Y'); ?></data>
                                            </div>
                                            <div class="hidden" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                                                <span itemprop="name">Por <?php the_author_posts_link(); ?></span>
                                            </div>
                                            <div class="desc-post" itemprop="description">
                                                <p><?php the_content_limit(185); ?></p>
                                            </div>
                                            <div class="btn gradient">
                                                <a href="<?php the_permalink(); ?>"><span>Continuar Lendo</span></a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <?php
                            endwhile;
                        endif;

                         ?>
                         <div class="col-xs-12">
                            <div class="navegacao">
                                <?php
                                    if (function_exists(custom_pagination)):
                                        custom_pagination($ultimos_posts->max_num_pages,"",$paged);
                                    endif;

                                    wp_reset_query();
                                ?>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
                <?php include(TEMPLATEPATH . '/template-parts/sidebar-blog.php'); ?>
            </div>
        </div>
    </div>
</div>
<?php
    if($paged < 2){
        session_start();
        $_SESSION["posts_exibidos"] = $posts_exibidos;
    }
?>
<?php get_footer(); ?>