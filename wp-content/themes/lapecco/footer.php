			</div><!-- FIM CONTEÚDO -->
		</div><!-- /#wrap -->
		<footer class="footer" itemscope itemtype="http://schema.org/Organization">
			<div class="my-container">
				<div class="row">
					<div class="col-xs-12 col-md-7">
						<div class="news">
							<span class="title-foot">Assine nossa newsletter</span>
							<!-- <form id="form-news" action="" method="post">
								<input type="email" name="email" placeholder="Seu melhor e-mail">
								<input type="hidden" value="AgenciaAssoweb" name="uri">
								<input type="hidden" name="loc" value="pt_BR">
								<input type="submit" value="Enviar">
							</form> -->

							<!--
							Do not modify the NAME value of any of the INPUT fields
							the FORM action, or any of the hidden fields (eg. input type=hidden).
							These are all required for this form to function correctly.
							-->
							<form method="post" action="http://mkt1.vhtelecom.com:8015/form.php?form=1" id="frmSS1" onsubmit="return CheckForm1(this);">
								<input type="email" name="email" value="" />
								<input type="hidden" name="format" value="h" />
								<input type="submit" value="Enviar" />
							</form>

							<script type="text/javascript">
							// <![CDATA[
								function CheckMultiple1(frm, name) {
									for (var i=0; i < frm.length; i++)
									{
										fldObj = frm.elements[i];
										fldId = fldObj.id;
										if (fldId) {
											var fieldnamecheck=fldObj.id.indexOf(name);
											if (fieldnamecheck != -1) {
												if (fldObj.checked) {
													return true;
												}
											}
										}
									}
									return false;
								}
								function CheckForm1(f) {
									var email_re = /[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i;
									if (!email_re.test(f.email.value)) {
										alert("Digite seu e-mail.");
										f.email.focus();
										return false;
									}

										return true;
									}
							// ]]>
							</script>





						</div>
						<div class="endereco">
							<span class="title-foot">Grupo de saúde lapecco</span>
							<b>Endereço da Matriz:</b>
							<span>Rua Jacuí, 1204 - Bairro Floresta</span>
							<span>Belo Horizonte - Minas Gerais</span>
							<div class="bx-link-fili">
								<a class="link-fili" href="<?php echo SITEURL ?>/filiais-para-laboratorios">Filiais do Laboratório</a>
							</div>
							<div class="tele">
								<span id="teleLabel"><b>Telefone:</b> (31) 3449-8500</span>
								<span><b>E-mail:</b> faleconosco@nucleolapecco.com.br</span>
							</div>
							<div class="btn gradient ver-mapaFooter">
								<a href="#"><span>Veja o mapa</span></a>
							</div>
						</div>
						<div class="serv">
							<span class="title-foot">Nossos Serviços</span>
							<?php
						      	wp_nav_menu(
						      		array(
								        'menu'            => 'Footer',
								 		'depth'           => 1,
								        'container_id'    => '',
								        'menu_class'      => 'cd-primary-nav',
								        'menu_id'         => 'cd-primary-nav',
								        'echo'            => true,
								        'before'          => '',
								        'after'           => '',
								        'link_before'     => '',
								        'link_after'      => '',
								        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								        'walker'        => new themeslug_walker_nav_menu
						      		)
						      	);
					      	?>
						</div>
					</div>
					<div class="col-xs-12 col-md-5 bx-face">
						<div class="fb-page" data-href="https://www.facebook.com/lapeccoodonto/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/lapeccoodonto/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/lapeccoodonto/">Núcleo Odontológico Lapecco</a></blockquote></div>
					</div>
					<?php
					$argsServPage = array(
				        'post__in' => array( 458 ),
				        'post_type' => 'page'
				    );
				    $queryServPage = new WP_Query( $argsServPage );

				    $queryServPage->the_post();

					$location = get_field('mapa');
					if( !empty($location) ): ?>
					    <div class="mapaFooter" id="mapaFooter">
					        <div class="marker" id="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
					    </div>
					<?php endif; ?>
					<div class="col-xs-12 div-foot">
						<hr>
						<div class="social">
							<?php
							   	$home = array('post_type' => 'page', 'page_id' => 2 ); // 2 = front-page
							    $home = new WP_Query( $home );
							    while ( $home->have_posts() ) : $home->the_post();
							    	if (get_field('facebook'))
							    		echo '<a href="'.get_field('facebook').'" target="_blank"><i class="icon-facebook-squared"></i></a>';
							    	if (get_field('twitter'))
							    		echo '<a href="'.get_field('twitter').'" target="_blank"><i class="icon-twitter"></i></a>';
							    	if (get_field('youtube'))
							    		echo '<a href="'.get_field('youtube').'" target="_blank"><i class="icon-youtube-play"></i></a>';
							    	if (get_field('instagram'))
							    		echo '<a href="'.get_field('instagram').'" target="_blank"><i class="icon-instagram"></i></a>';
							    	if (get_field('google_plus'))
							    		echo '<a href="'.get_field('google_plus').'" target="_blank"><i class="icon-gplus"></i></a>';
								endwhile;
				    			wp_reset_query();
				    		?>
						</div>
				      	<div class="copy">
				        	<span itemprop="copyrightHolder">&copy; Copyright 2001/<?php echo date('Y') ?>. Todos os direitos reservados.</span>
				       	</div>
				    	<div class="assoweb">
				    		<a href="http://assoweb.com.br/" target="_blank">
				    			<img src="<?php echo THEMEURL ?>/assets/img/agencia-assoweb.png" alt="">
				    		</a>
				    	</div>
				    </div>
				</div>
			</div>


			<div class="hidden" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<h3 itemprop="name">Grupo Lapecco</h3>
				<address>
					<span itemprop="streetAddress">Rua Jacuí, 1204 - Bairro Floresta </span><br/>
					<span itemprop="addressLocality">Belo Horizonte - MG</span><br/>
					<span itemprop="postalCode">CEP: 31.110-050</span>
				</address>
				<span itemprop="telephone">(31) 3449-8500</span><br/>
				<span itemprop="email"><?php echo antispambot('contato@domcontabilidade.com.br'); ?></span>
				<div itemprop="location" itemscope itemtype="http://schema.org/Place">
		          	<span class="geo" itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
		            	<span itemprop="latitude" class="latitude">-19.902715</span>
		            	<span itemprop="longitude" class="longitude">-43.932879</span>
		          	</span>
		        </div>
		    </div>
		</footer>
		<a href="#0" class="cd-top">Topo</a>


		<!-- MODAL Orçamento -->
        <div class="mask" role="dialog"></div>
        <div class="modal-agendamento popup" role="alert">
            <div class="head-modal">
                <button class="closeAgend"><i class="icon-cancel"></i></button>
            </div>
            <div class="body-modal">
            	<h3>Agende uma consulta/exame</h3>
            	<div class="labelOrc"></div>
            	<?php echo do_shortcode('[contact-form-7 id="358" title="Agendamento"]'); ?>
            </div>
			<div class="foot-modal">
                <div class="location">
                    <i class="icon-phone-squared"></i><span class="telMenuSpan"> (31) 3449-8500</span>
                </div>
            </div>
        </div>

		<div id="ajx" data-url="<?php echo admin_url('admin-ajax.php') ?>"></div>
		<?php wp_footer();
			if (is_post_type_archive('odontologia') || is_singular('odontologia')){ ?>
				<script>
					(function($) {
						$('.telMenu a').text('31 3449-8503 Odontologia').attr('href','tel:+553134498503');
						$('.telMenuSpan').text('31 3449-8503');
						$('#teleLabel').html('<b>Telefone:</b> (31) 3449-8503');
						$('.labelOrc').html('<h4>Orçamento sem compromisso</h4>');
						$('.modal-agendamento .body-modal h3').css('margin-bottom','5px');
					})(jQuery);
				</script>
			<?php } ?>

	</body>
</html>