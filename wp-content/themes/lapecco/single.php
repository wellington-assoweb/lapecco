<?php get_header();
    include(TEMPLATEPATH . '/template-parts/header-blog.php');
?>

<?php while ( have_posts() ) : the_post(); ?>

<section class="box-blog single">
    <!-- RESTANTE BLOG-->
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-lg-9">
                <article class="one-atricle" itemscope itemtype="http://schema.org/NewsArticle">
                    <h1 class="title"><?php the_title();?></h1>
                    <div class="name-data">
                        <div class="data">
                            <data itemprop="datePublished" value="<?php the_time('j \d\e F \d\e Y'); ?>"><?php the_time('j \d\e F \d\e Y'); ?></data>
                        </div>
                    </div>
                    <?php
                    $img_title = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
                    if ($img_title[0]): ?>
                        <div class="bx-img bg-destak-<?php echo $id; ?>"></div>
                    <?php endif; ?>
                    <div class="conteudo-blog">
                        <?php the_content();?>
                        <p class="assi-post"><i>Artigo redigido pela equipe do Grupo de Saúde Lapecco.</i></p>
                    </div>
                </article>
                <div class="compartilhamento">
                    <?php echo do_shortcode( '[jpshare]' );?>
                </div>
                <div class="comentarios" id="disqus_thread">
                    <?php disqus_embed('agenciaassoweb'); ?>
                </div>
                <div class="posts-rest">
                    <h2 class="title">Você também pode se interessar por...</h2>
                    <div class="row">
                    <?php
                        $getPostRelacSingle = $_SESSION["postRelacSingle"];

                        if($getPostRelacSingle->have_posts()) :
                            while( $getPostRelacSingle->have_posts() ):
                                $getPostRelacSingle->the_post();

                    ?>
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <article class="one-article" itemscope itemtype="http://schema.org/NewsArticle">
                                        <div class="categ">
                                            <?php
                                            $categoria = get_the_category();
                                            foreach($categoria as $category) {
                                                $output = '<a class="nome-categoria" href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "Ver todos os posts de %s" ), $category->name ) ) . '"><span>'.$category->cat_name.'</span></a>';
                                            }
                                            echo $output; ?>
                                        </div>
                                        <a href="<?php the_permalink(); ?>">
                                            <figure itemprop="image" class="post-relac-<?php echo $id; ?>">
                                                <figcaption class="hidden">
                                                    <h4><?php the_title(); ?></h4>
                                                </figcaption>
                                            </figure>
                                        </a>
                                        <div class="description">
                                            <h3 class="title-post" itemprop="headline">
                                                <a href="<?php the_permalink(); ?>"><?php the_title_limit($post->Id, 55); ?></a>
                                            </h3>
                                            <div class="date">
                                                <data itemprop="datePublished" value="<?php the_time('j \d\e F \d\e Y'); ?>"><?php the_time('j \d\e F \d\e Y'); ?></data>
                                            </div>
                                            <div class="hidden" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                                                <span itemprop="name">Por <?php the_author_posts_link(); ?></span>
                                            </div>
                                            <div class="desc-post" itemprop="description">
                                                <p><?php the_content_limit(185); ?></p>
                                            </div>
                                            <div class="btn gradient">
                                                <a href="<?php the_permalink(); ?>"><span>Continuar Lendo</span></a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <?php
                            endwhile;
                        else:
                            wp_reset_query();

                            $getPostRelacSingle = $_SESSION["postRelacSingle"];
                            while( $getPostRelacSingle->have_posts() ):
                                $getPostRelacSingle->the_post();
                        ?>
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <article class="one-article" itemscope itemtype="http://schema.org/NewsArticle">
                                        <div class="categ">
                                            <?php
                                            $categoria = get_the_category();
                                            foreach($categoria as $category) {
                                                $output = '<a class="nome-categoria" href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "Ver todos os posts de %s" ), $category->name ) ) . '"><span>'.$category->cat_name.'</span></a>';
                                            }
                                            echo $output; ?>
                                        </div>
                                        <a href="<?php the_permalink(); ?>">
                                            <figure itemprop="image" class="post-relac-<?php echo $id; ?>">
                                                <figcaption class="hidden">
                                                    <h4><?php the_title(); ?></h4>
                                                </figcaption>
                                            </figure>
                                        </a>
                                        <div class="description">
                                            <h3 class="title-post" itemprop="headline">
                                                <a href="<?php the_permalink(); ?>"><?php the_title_limit($post->Id, 55); ?></a>
                                            </h3>
                                            <div class="date">
                                                <data itemprop="datePublished" value="<?php the_time('j \d\e F \d\e Y'); ?>"><?php the_time('j \d\e F \d\e Y'); ?></data>
                                            </div>
                                            <div class="hidden" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                                                <span itemprop="name">Por <?php the_author_posts_link(); ?></span>
                                            </div>
                                            <div class="desc-post" itemprop="description">
                                                <p><?php the_content_limit(185); ?></p>
                                            </div>
                                            <div class="btn gradient">
                                                <a href="<?php the_permalink(); ?>"><span>Continuar Lendo</span></a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-lg-3">
                <?php include(TEMPLATEPATH . '/template-parts/sidebar-blog.php'); ?>
            </div>
        </div>
    </div><!-- container -->
</section>
<?php endwhile?>


<?php get_footer(); ?>