<?php get_header(); ?>

<section class="head-title bg-header-<?php echo $post->ID; ?>">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="page-name">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                    } ?>
                    <h1 class="title-princ"><?php the_title(); ?></h1>
                </div>
                <?php
                    $icone_cabecalho_do_servico = get_field('icone_cabecalho_do_servico');
                ?>

                <img class="icone" src="<?php echo $icone_cabecalho_do_servico['url'] ?>" alt="<?php echo $icone_cabecalho_do_servico['alt'] ?>">
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="bx-cont">
                    <h2 class="title-custom"><?php if(get_field('titulo_box_branco')){ echo get_field('titulo_box_branco'); }else{ echo 'Mais do que uma clínica de saúde, somos um grupo completo.'; } ?></h2>
                    <?php echo get_field('conteudo_box_branco'); ?>
                    <div class="btn center btn gradient center">
                        <a href="<?php echo SITEURL; ?>/resultados-online"><span>Resultados Online</span></a>
                    </div>
                    <div class="btn center btn-fili">
                        <a href="#filiais"><span>Conheça as filias</span></a>
                    </div>
                </div>
                <div class="btn gradient center btn-voltar btnagend">
                    <a href="#"><span><?php if(get_field('texto_do_botão_do_cabecalho')) { echo get_field('texto_do_botão_do_cabecalho'); }else{ echo 'Agende seu exame'; }?></span></a>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="desc-princ desc-prim">
    <div class="my-container">
        <div class="row">
            <div class="bx-cont">
                <div class="col-xs-12">
                    <h2 class="title"><?php echo get_field('titulo_sessao_um'); ?></h2>
                    <h3 class="subtitle"><?php echo get_field('subtitulo_sessão_um'); ?></h3>
                    <?php echo get_field('conteudo_sessao_um'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="perg-e-res">
    <div class="my-container">
        <div class="row ctrl-sing">
            <div class="col-sm-12 col-md-3 ctrl-nav-per">
                <div class="nav-perguntas">
                    <nav>
                        <ul>
                            <li class="item">
                                <a href="script:void(0)" id="exames" class="titulo-perguntas-frequentes active-menu">
                                    </span>Exames
                                </a>
                            </li>
                            <li class="item">
                                <a href="script:void(0)" id="exames_de_urina" class="titulo-perguntas-frequentes">
                                    </span>Exames de Urina
                                </a>
                            </li>
                            <li class="item">
                                <a href="script:void(0)" id="exames_de_fezes" class="titulo-perguntas-frequentes">
                                    </span>Exames de Fezes
                                </a>
                            </li>
                            <li class="item">
                                <a href="script:void(0)" id="exames_de_imagem" class="titulo-perguntas-frequentes">
                                    </span>Exames de imagem
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <?php
                $argsServPage = array(
                    'post__in' => array( 889 ),
                    'post_type' => 'page'
                );
                $queryServPage = new WP_Query( $argsServPage );

                $queryServPage->the_post();
            ?>
            <div class="col-sm-12 col-md-9">
                <form>
                    <div class="lista-perg exames perg-active">
                        <h3 class="sub-title-perg">Exames</h3>
                        <?php $count = 1; ?>
                        <?php while( have_rows('exames') ): the_row();
                            // vars
                            $pergunta = get_sub_field('pergunta');
                            $resposta = get_sub_field('resposta');
                        ?>
                        <input type="radio" id="radio-option-<?php echo $count; ?>" name="accordion-radios">
                        <div class="baffle">
                            <div class="perguntas-frequentes">
                                <label for="radio-option-<?php echo $count; ?>" title="Ver resposta"><?php echo $pergunta ?></label>
                            </div>
                            <div class="respostas-frequentes">
                                <?php echo $resposta ?>
                            </div>
                        </div>
                        <?php $count++; endwhile; ?>
                    </div>

                    <div class="lista-perg exames_de_urina">
                        <h3 class="sub-title-perg">Exames de Urina</h3>
                        <?php while( have_rows('exames_de_urina') ): the_row();
                            // vars
                            $pergunta = get_sub_field('pergunta');
                            $resposta = get_sub_field('resposta');
                        ?>
                        <input type="radio" id="radio-option-<?php echo $count; ?>" name="accordion-radios">
                        <div class="baffle">
                            <div class="perguntas-frequentes">
                                <label for="radio-option-<?php echo $count; ?>" title="Ver resposta"><?php echo $pergunta ?></label>
                            </div>
                            <div class="respostas-frequentes">
                                <?php echo $resposta ?>
                            </div>
                        </div>
                        <?php $count++; endwhile; ?>
                    </div>

                    <div class="lista-perg exames_de_fezes">
                        <h3 class="sub-title-perg">Exames de Fezes</h3>
                        <?php while( have_rows('exames_de_fezes') ): the_row();
                            // vars
                            $pergunta = get_sub_field('pergunta');
                            $resposta = get_sub_field('resposta');
                        ?>
                        <input type="radio" id="radio-option-<?php echo $count; ?>" name="accordion-radios">
                        <div class="baffle">
                            <div class="perguntas-frequentes">
                                <label for="radio-option-<?php echo $count; ?>" title="Ver resposta"><?php echo $pergunta ?></label>
                            </div>
                            <div class="respostas-frequentes">
                                <?php echo $resposta ?>
                            </div>
                        </div>
                        <?php $count++; endwhile; ?>
                    </div>

                    <div class="lista-perg exames_de_imagem">
                        <h3 class="sub-title-perg">Exames de imagem</h3>
                        <?php while( have_rows('exames_de_imagem') ): the_row();
                            // vars
                            $pergunta = get_sub_field('pergunta');
                            $resposta = get_sub_field('resposta');
                        ?>
                        <input type="radio" id="radio-option-<?php echo $count; ?>" name="accordion-radios">
                        <div class="baffle">
                            <div class="perguntas-frequentes">
                                <label for="radio-option-<?php echo $count; ?>" title="Ver resposta"><?php echo $pergunta ?></label>
                            </div>
                            <div class="respostas-frequentes">
                                <?php echo $resposta ?>
                            </div>
                        </div>
                        <?php $count++; endwhile; wp_reset_query(); ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="desc-princ desc-seg">
    <div class="my-container">
        <div class="row">
            <div class="bx-cont">
                <div class="btn gradient btnagend">
                    <a href="#"><span>Agende seu exame</span></a>
                </div>
                <div class="col-xs-12" id="filiais">
                    <hr>
                    <h3><b>Filiais </b></h3>
                </div>

                <div class="col-xs-12 col-sm-6 bx-fili">
                    <p>Unidade Santa Monica:</p>

                    <p>Av. Desembargador Felippe Immesi, 22   (ref. Final da Antiga Av. 12 de Outubro)<br>
                    Bairro Santa Monica - BH/MG<br>
                    Tel: 3457-7526</p>
                </div>


                <div class="col-xs-12 col-sm-6 bx-fili">
                    <p>Unidade Venda Nova I</p>

                    <p>Rua Padre Pedro Pinto, 554 lj 01
                    Venda Nova - BH/MG
                    Tel: 3457-8888</p>
                </div>

                <div class="col-xs-12 col-sm-6 bx-fili">
                    <p>Unidade Venda Nova II</p>

                    <p>Rua Santo Antônio, 205
                    Venda Nova - BH/MG
                    Tel: 3457-4541</p>
                </div>
                <div class="col-xs-12 col-sm-6 bx-fili">
                    <p>Unidade Justinópolis</p>

                    <p>Av. Denise Cristina Rocha, 857 - 3º andar - sl 310  (Shopping Justinópolis)
                    Justinópolis/Ribeirão das Neves
                    Tel: 3638-2022</p>
                </div>

                <div class="col-xs-12">
                    <h3>Concursos</h3>
                    <p>Participamos da realização de exames laboratoriais e complementares de diagnóstico há mais 25 anos para o IPSM (Policia Militar e Corpo de Bombeiros), Polícia Civil, Agente Penitenciário, Polícia Federal e outros.</p>
                    <div class="btn btn-conc">
                        <a href="<?php echo SITEURL; ?>/concursos/concursos/"><span>Concuros</span></a>
                    </div>
                </div>

                <div class="col-xs-12">
                    <h3>Resultados Online</h3>
                    <p><a href="<?php echo SITEURL; ?>/resultados-online/">Clique aqui</a> para acessar os resultados online do seu exame.</p>
                </div>

            </div>
        </div>
    </div>
</section>

<?php
    $argsServPage = array(
        'post__in' => array( 419 ),
        'post_type' => 'page'
    );
    $queryServPage = new WP_Query( $argsServPage );

    $queryServPage->the_post();
    $fundo_slide_exames_laboratoriais = get_field('fundo_slide_exames_laboratoriais');
?>
    <section class="slide-full-wide bg-slide-<?php echo $fundo_slide_exames_laboratoriais['ID']; ?>">
        <div class="my-container">
            <div class="row">
                <div class="bx-cont">
                    <h3 class="title-custom"><?php echo get_field('titulo_slide_exames_laboratoriais'); ?></h3>
                    <?php echo get_field('conteudo_slide_exames_laboratoriais'); ?>
                </div>
            </div>
        </div>
    </section>
<?php
    wp_reset_query();
?>


<?php
    $countDelay = 0.0;
    $argsOdont = array(
        'post_type'         => 'exames-laboratoriais',
        'posts_per_page'    => -1,
        'post__not_in'      => array($post->ID)
    );
    $odont = new WP_Query( $argsOdont );
    if($odont->have_posts()) :
?>
        <section class="cons slide">
            <div class="my-container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="title">Serviços de Exames Laboratoriais em BH</h3>
                    </div>
                    <div class="col-xs-12">
                        <div class="owl-carousel">
                            <?php
                                while( $odont->have_posts() ) {
                                    $odont->the_post();
                                    $imagem_da_chamada_do_servico = get_field('imagem_da_chamada_do_servico');
                            ?>
                                    <div class="item wow fadeInUp" data-wow-duration="1s" data-wow-delay="<?php echo $countDelay ?>s">
                                        <a href="<?php the_permalink(); ?>"></a>
                                        <div class="bx-img">
                                            <img src="<?php echo $imagem_da_chamada_do_servico['url'] ?>" alt="">
                                        </div>
                                        <span><?php the_title(); ?></span>
                                    </div>
                            <?php
                                    $countDelay = $countDelay + 0.2;
                                }
                                wp_reset_query();
                            ?>
                        </div>
                        <div class="btn gradient center">
                            <a href="<?php echo SITEURL ?>/exames-laboratoriais"><span>Todos os Serviços</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<?php endif; ?>


<?php get_footer(); ?>
<script>
    (function($) {
        var $doc = $('html, body'),
        ctrlNav = $('.btn-fili a');
        ctrlNav.click(function() {
            $doc.animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top - 150
            }, 500);
            return false;
        });
    })(jQuery);
</script>