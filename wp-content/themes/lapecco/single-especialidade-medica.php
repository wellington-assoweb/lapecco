<?php get_header(); ?>

<section class="head-title bg-header-<?php echo $post->ID; ?>">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="page-name">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                    } ?>
                    <h1 class="title-princ"><?php the_title(); ?></h1>

                </div>
                <?php
                    $icone_cabecalho_do_servico = get_field('icone_cabecalho_do_servico');
                ?>

                <img class="icone" src="<?php echo $icone_cabecalho_do_servico['url'] ?>" alt="<?php echo $icone_cabecalho_do_servico['alt'] ?>" >
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="bx-cont">
                    <h2 class="title-custom"><?php echo get_field('titulo_box_branco'); ?></h2>
                    <?php echo get_field('conteudo_box_branco'); ?>
                    <div class="btn gradient center btnagend">
                        <a href="#"><span><?php if(get_field('texto_do_botão_do_cabecalho')) { echo get_field('texto_do_botão_do_cabecalho'); }else{ echo 'Agende uma Consulta'; }?></span></a>
                    </div>
                </div>
                <div class="btn center btn-voltar">
                    <a href="<?php echo SITEURL ?>/especialidade-medica"><span>Voltar para os serviços</span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="desc-princ">
    <div class="my-container">
        <div class="row">
            <div class="bx-cont">
                <h2 class="title"><?php echo get_field('titulo_sessao_um'); ?></h2>
                <h3 class="subtitle"><?php echo get_field('subtitulo_sessão_um'); ?></h3>
                <?php echo get_field('conteudo_sessao_um'); ?>

                <div class="btn gradient">
                    <div class="btnagend">
                        <a href="#"><span><?php if(get_field('texto_do_botao_sessao_um')){ echo get_field('texto_do_botao_sessao_um'); }else{echo 'Agende uma consulta'; } ?></span></a>
                    </div>
                    <div>
                        <a class="btn-voltar" href="<?php echo SITEURL ?>/especialidade-medica"><span>Voltar para os serviços</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
     $argsServPage = array(
        'post__in' => array( 419 ),
        'post_type' => 'page'
    );
    $queryServPage = new WP_Query( $argsServPage );

    $queryServPage->the_post();
    $fundo_slide_especialidade_medica = get_field('fundo_slide_especialidade_medica');
?>
    <section class="slide-full-wide bg-slide-<?php echo $fundo_slide_especialidade_medica['ID']; ?>">
        <div class="my-container">
            <div class="row">
                <div class="bx-cont">
                    <h3 class="title-custom"><?php echo get_field('titulo_slide_especialidade_medica'); ?></h3>
                    <?php echo get_field('conteudo_slide_especialidade_medica'); ?>
                </div>
            </div>
        </div>
    </section>
<?php
    wp_reset_query();
    include(TEMPLATEPATH . '/template-parts/slide-consultas.php');
    get_footer();
?>