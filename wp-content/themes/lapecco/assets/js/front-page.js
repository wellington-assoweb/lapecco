(function($) {
	var width = $(window).width();
	if(width > 650-17){
		listBanner = $('.nav-banner a');
		listBannerCount = $('.nav-banner a').length;
		click = false;
		listBanner.each(function(index, el) {
			var idElement = $(this).data('id');
			time = (index+1) * 5000;
			id = $(this).data('id');
			myVar = setTimeout(function(){
				if(click == false){
					$('.nav-banner a').removeClass('active');
					$('.img-esq').removeClass('active');
					$('.box-cont').removeClass('active');

					$('.nav-banner a[data-id="ctrlB'+index+'"]').addClass('active');
					$('#ctrlB'+index).addClass('active');
					$('.ctrlB'+index).addClass('active');
				}
			}, time);
		});

		$('.nav-banner a').click(function(e){
			click = true;
			e.preventDefault();
			$('.nav-banner a').removeClass('active');
			$('.img-esq').removeClass('active');

			id = $(this).data('id');
			$('.box-cont').removeClass('active');

			$(this).addClass('active');
			$('#'+id).addClass('active');
			$('.'+id).addClass('active');

			clearInterval(myVar);
		});
	}

	// Slide Odontologia
	var owlClientes = $('.odon .owl-carousel');
	owlClientes.owlCarousel({
		autoplayTimeout: 7000,
		responsiveClass: true,
        smartSpeed: 750,
        dots: false,
        margin: 20,
        loop: true,
        nav: true,
		responsive:{
			0:{
		    	items: 1
			},
			481:{
		    	items: 2
			},
			790:{
			    items: 3
			},
			991:{
		    	items: 4
			}
		},
		navText: ['<i class="icon-left-open"></i>','<i class="icon-right-open"></i>']
	});

	// Slide Consultas
	var owlClientes = $('.cons .owl-carousel');
	owlClientes.owlCarousel({
		autoplayTimeout: 7000,
		responsiveClass: true,
        smartSpeed: 750,
        dots: false,
        margin: 20,
        loop: true,
        nav: true,
		responsive:{
			0:{
		    	items: 1
			},
			481:{
		    	items: 2
			},
			790:{
			    items: 3
			},
			991:{
		    	items: 4
			}
		},
		navText: ['<i class="icon-left-open"></i>','<i class="icon-right-open"></i>']
	});


	// Slide Exames Laboratoriais
	var owlClientes = $('.exa-lab .owl-carousel');
	owlClientes.owlCarousel({
		autoplayTimeout: 7000,
		responsiveClass: true,
        smartSpeed: 750,
        dots: false,
        margin: 20,
        loop: true,
        nav: true,
		responsive:{
			0:{
		    	items: 1
			},
			481:{
		    	items: 2
			},
			790:{
			    items: 3
			},
			991:{
		    	items: 4
			}
		},
		navText: ['','']
	});

	// Slide Exames Laboratoriais
	var owlClientes = $('.exa-img .owl-carousel');
	owlClientes.owlCarousel({
		autoplayTimeout: 7000,
		responsiveClass: true,
        smartSpeed: 750,
        dots: false,
        margin: 20,
        loop: true,
        nav: true,
		responsive:{
			0:{
		    	items: 1
			},
			481:{
		    	items: 2
			},
			790:{
			    items: 3
			},
			991:{
		    	items: 4
			}
		},
		navText: ['<i class="icon-left-open"></i>','<i class="icon-right-open"></i>']
	});

    $('#tipoServicos2').change(function(){ // When btn is pressed.
        var ajaxUrl = $('#ajx').data('url');
        var competencia = $("select[name=tipo-servicos2]").val();
        $("#msgAuto").empty();
        beforeSend: $("#completeConve").attr({"placeholder":"Carregando Convênios...","value":"" });
        beforeSend: $(".svgLoad").css('visibility','visible');

        $.post(ajaxUrl, {
            action:"get_convenios_ajax",
            competencia: competencia

        }).success(function(posts){
	        beforeSend: $("#completeConve").attr("placeholder","Digite para verificar seu convênio...").removeAttr('disabled');
	        beforeSend: $(".svgLoad").css('visibility','hidden');
            var postsArray = posts.split('|');

            var availableTags = [];
            $( "#completeConve" ).autocomplete({
              	source: postsArray,
              	response: function(event, ui) {
		            if (ui.content.length === 0) {
		                $("#msgAuto").text("Nenhum resultado encontrado.");
		            } else {
		                $("#msgAuto").empty();
		            }
		        }
            });

        });
   });

})(jQuery);