(function($) {
	// Slide Odontologia
	var owlClientes = $('.bx-gray .owl-carousel');
	owlClientes.owlCarousel({
		autoplayTimeout: 7000,
		responsiveClass: true,
        smartSpeed: 750,
        autoplay: true,
        dots: true,
        loop: true,
        items:1
	});


	// Slide Consultas
	var owlClientes = $('.cons .owl-carousel');
	owlClientes.owlCarousel({
		autoplayTimeout: 7000,
		responsiveClass: true,
        smartSpeed: 750,
        dots: false,
        margin: 20,
        loop: true,
        nav: true,
		responsive:{
			0:{
		    	items: 1
			},
			481:{
		    	items: 2
			},
			790:{
			    items: 3
			},
			991:{
		    	items: 4
			}
		},
		navText: ['<i class="icon-left-open"></i>','<i class="icon-right-open"></i>']
	});
})(jQuery);


