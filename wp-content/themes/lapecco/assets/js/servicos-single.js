(function($) {
	// Slide Consultas
	var owlClientes2 = $('.cons .owl-carousel');
	owlClientes2.owlCarousel({
		autoplayTimeout: 7000,
		responsiveClass: true,
        smartSpeed: 750,
        autoplay: true,
        dots: false,
        margin: 20,
        loop: true,
        nav: true,
		responsive:{
			0:{
		    	items: 1
			},
			481:{
		    	items: 2
			},
			790:{
			    items: 3
			},
			991:{
		    	items: 4
			}
		},
		navText: ['<i class="icon-left-open"></i>','<i class="icon-right-open"></i>']
	});

	var countSlide = $('.cons .item').length;
	if(countSlide <= 4 ){
		$('.cons .owl-controls').css('display','none');
	}





	$(document).ready(function(){
		$('.slide-full-wide .owl-controls').addClass('my-container');
	});

})(jQuery);