(function($) {
    jQuery(document).ready(function($) {
        $('.assinar').click(function(e) {
            e.preventDefault();
            $('#modalPersNews').addClass('active');
        });
        $('#closeModal').click(function(e) {
            $('#modalPersNews').removeClass('active');
        });
        $(".mask-news").click(function(){
            $('#modalPersNews').removeClass('active');
        });
        //CLOSE WHEN PRESS ESC
        $(document).keyup(function(e) {
            if (e.keyCode == 27) {
                if($(".mask, .mask-news").hasClass("active")){
                    closeModal();
                }
                $('#modalPersNews').removeClass('active');
            }
        });
    });
})(jQuery);
