"use strict";!function(t){"function"==typeof define&&define.amd?define(["jquery"],t):"object"==typeof exports?module.exports=t(require("jquery")):t(jQuery||Zepto)}(function(t){var a=function(a,e,n){var r={invalid:[],getCaret:function(){try{var t,e=0,n=a.get(0),s=document.selection,o=n.selectionStart;return s&&-1===navigator.appVersion.indexOf("MSIE 10")?(t=s.createRange(),t.moveStart("character",-r.val().length),e=t.text.length):(o||"0"===o)&&(e=o),e}catch(c){}},setCaret:function(t){try{if(a.is(":focus")){var e,n=a.get(0);n.setSelectionRange?(n.focus(),n.setSelectionRange(t,t)):(e=n.createTextRange(),e.collapse(!0),e.moveEnd("character",t),e.moveStart("character",t),e.select())}}catch(r){}},events:function(){a.on("keydown.mask",function(t){a.data("mask-keycode",t.keyCode||t.which)}).on(t.jMaskGlobals.useInput?"input.mask":"keyup.mask",r.behaviour).on("paste.mask drop.mask",function(){setTimeout(function(){a.keydown().keyup()},100)}).on("change.mask",function(){a.data("changed",!0)}).on("blur.mask",function(){c===r.val()||a.data("changed")||a.trigger("change"),a.data("changed",!1)}).on("blur.mask",function(){c=r.val()}).on("focus.mask",function(a){n.selectOnFocus===!0&&t(a.target).select()}).on("focusout.mask",function(){n.clearIfNotMatch&&!s.test(r.val())&&r.val("")})},getRegexMask:function(){for(var t,a,n,r,s,c,i=[],l=0;l<e.length;l++)t=o.translation[e.charAt(l)],t?(a=t.pattern.toString().replace(/.{1}$|^.{1}/g,""),n=t.optional,r=t.recursive,r?(i.push(e.charAt(l)),s={digit:e.charAt(l),pattern:a}):i.push(n||r?a+"?":a)):i.push(e.charAt(l).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&"));return c=i.join(""),s&&(c=c.replace(new RegExp("("+s.digit+"(.*"+s.digit+")?)"),"($1)?").replace(new RegExp(s.digit,"g"),s.pattern)),new RegExp(c)},destroyEvents:function(){a.off(["input","keydown","keyup","paste","drop","blur","focusout",""].join(".mask "))},val:function(t){var e,n=a.is("input"),r=n?"val":"text";return arguments.length>0?(a[r]()!==t&&a[r](t),e=a):e=a[r](),e},getMCharsBeforeCount:function(t,a){for(var n=0,r=0,s=e.length;s>r&&t>r;r++)o.translation[e.charAt(r)]||(t=a?t+1:t,n++);return n},caretPos:function(t,a,n,s){var c=o.translation[e.charAt(Math.min(t-1,e.length-1))];return c?Math.min(t+n-a-s,n):r.caretPos(t+1,a,n,s)},behaviour:function(e){e=e||window.event,r.invalid=[];var n=a.data("mask-keycode");if(-1===t.inArray(n,o.byPassKeys)){var s=r.getCaret(),c=r.val(),i=c.length,l=r.getMasked(),u=l.length,f=r.getMCharsBeforeCount(u-1)-r.getMCharsBeforeCount(i-1),h=i>s;return r.val(l),h&&(8!==n&&46!==n&&(s=r.caretPos(s,i,u,f)),r.setCaret(s)),r.callbacks(e)}},getMasked:function(t,a){var s,c,i=[],l=void 0===a?r.val():a+"",u=0,f=e.length,h=0,v=l.length,d=1,k="push",p=-1;for(n.reverse?(k="unshift",d=-1,s=0,u=f-1,h=v-1,c=function(){return u>-1&&h>-1}):(s=f-1,c=function(){return f>u&&v>h});c();){var g=e.charAt(u),m=l.charAt(h),M=o.translation[g];M?(m.match(M.pattern)?(i[k](m),M.recursive&&(-1===p?p=u:u===s&&(u=p-d),s===p&&(u-=d)),u+=d):M.optional?(u+=d,h-=d):M.fallback?(i[k](M.fallback),u+=d,h-=d):r.invalid.push({p:h,v:m,e:M.pattern}),h+=d):(t||i[k](g),m===g&&(h+=d),u+=d)}var y=e.charAt(s);return f!==v+1||o.translation[y]||i.push(y),i.join("")},callbacks:function(t){var s=r.val(),o=s!==c,i=[s,t,a,n],l=function(t,a,e){"function"==typeof n[t]&&a&&n[t].apply(this,e)};l("onChange",o===!0,i),l("onKeyPress",o===!0,i),l("onComplete",s.length===e.length,i),l("onInvalid",r.invalid.length>0,[s,t,a,r.invalid,n])}};a=t(a);var s,o=this,c=r.val();e="function"==typeof e?e(r.val(),void 0,a,n):e,o.mask=e,o.options=n,o.remove=function(){var t=r.getCaret();return r.destroyEvents(),r.val(o.getCleanVal()),r.setCaret(t-r.getMCharsBeforeCount(t)),a},o.getCleanVal=function(){return r.getMasked(!0)},o.getMaskedVal=function(t){return r.getMasked(!1,t)},o.init=function(e){if(e=e||!1,n=n||{},o.clearIfNotMatch=t.jMaskGlobals.clearIfNotMatch,o.byPassKeys=t.jMaskGlobals.byPassKeys,o.translation=t.extend({},t.jMaskGlobals.translation,n.translation),o=t.extend(!0,{},o,n),s=r.getRegexMask(),e===!1){n.placeholder&&a.attr("placeholder",n.placeholder),a.data("mask")&&a.attr("autocomplete","off"),r.destroyEvents(),r.events();var c=r.getCaret();r.val(r.getMasked()),r.setCaret(c+r.getMCharsBeforeCount(c,!0))}else r.events(),r.val(r.getMasked())},o.init(!a.is("input"))};t.maskWatchers={};var e=function(){var e=t(this),r={},s="data-mask-",o=e.attr("data-mask");return e.attr(s+"reverse")&&(r.reverse=!0),e.attr(s+"clearifnotmatch")&&(r.clearIfNotMatch=!0),"true"===e.attr(s+"selectonfocus")&&(r.selectOnFocus=!0),n(e,o,r)?e.data("mask",new a(this,o,r)):void 0},n=function(a,e,n){n=n||{};var r=t(a).data("mask"),s=JSON.stringify,o=t(a).val()||t(a).text();try{return"function"==typeof e&&(e=e(o)),"object"!=typeof r||s(r.options)!==s(n)||r.mask!==e}catch(c){}},r=function(t){var a,e=document.createElement("div");return t="on"+t,a=t in e,a||(e.setAttribute(t,"return;"),a="function"==typeof e[t]),e=null,a};t.fn.mask=function(e,r){r=r||{};var s=this.selector,o=t.jMaskGlobals,c=o.watchInterval,i=r.watchInputs||o.watchInputs,l=function(){return n(this,e,r)?t(this).data("mask",new a(this,e,r)):void 0};return t(this).each(l),s&&""!==s&&i&&(clearInterval(t.maskWatchers[s]),t.maskWatchers[s]=setInterval(function(){t(document).find(s).each(l)},c)),this},t.fn.masked=function(t){return this.data("mask").getMaskedVal(t)},t.fn.unmask=function(){return clearInterval(t.maskWatchers[this.selector]),delete t.maskWatchers[this.selector],this.each(function(){var a=t(this).data("mask");a&&a.remove().removeData("mask")})},t.fn.cleanVal=function(){return this.data("mask").getCleanVal()},t.applyDataMask=function(a){a=a||t.jMaskGlobals.maskElements;var n=a instanceof t?a:t(a);n.filter(t.jMaskGlobals.dataMaskAttr).each(e)};var s={maskElements:"input,td,span,div",dataMaskAttr:"*[data-mask]",dataMask:!0,watchInterval:300,watchInputs:!0,useInput:r("input"),watchDataMask:!1,byPassKeys:[9,16,17,18,36,37,38,39,40,91],translation:{0:{pattern:/\d/},9:{pattern:/\d/,optional:!0},"#":{pattern:/\d/,recursive:!0},A:{pattern:/[a-zA-Z0-9]/},S:{pattern:/[a-zA-Z]/}}};t.jMaskGlobals=t.jMaskGlobals||{},s=t.jMaskGlobals=t.extend(!0,{},s,t.jMaskGlobals),s.dataMask&&t.applyDataMask(),setInterval(function(){t.jMaskGlobals.watchDataMask&&t.applyDataMask()},s.watchInterval)});

(function($) {
    // MÁSCARA DO TELEFONE
    var SPMaskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
      onKeyPress: function(val, e, field, options) {
          field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

    $('.tel').mask(SPMaskBehavior, spOptions);



    // UPDATE YOUR BROWSER
    var $buoop = {vs:{i:11,f:30,o:15,s:7},c:2};
    function $buo_f(){
        var e = document.createElement("script");
        e.src = "//browser-update.org/update.min.js";
        document.body.appendChild(e);
    };
    try {
        document.addEventListener("DOMContentLoaded", $buo_f,false)
    }catch(e){
        window.attachEvent("onload", $buo_f)
    }
    // END UPDATE YOUR BROWSER

    //SMALLER HEADER WHEN SCROLL PAGE
    $(window).scroll(function () {
         var sc = $(window).scrollTop()
        if (sc > 40) {
            $("#header-sroll").addClass("small")
        }else {
            $("#header-sroll").removeClass("small")
        }
    });
    //KEEP THE FOOTER IN FOOTER
    function rodape(){
        var footerHeight = $('.footer').height();
        // $('.footer').css('margin-top', -(footerHeight)+"px");
        $('.conteudo').css('padding-bottom', (footerHeight + 80)+"px");
    };
    $('.ui-datepicker-prev span').click(function(){
        console.log('oi A');
        $('.ui-datepicker-prev span').replaceWith('<span class="icon-left-open"></span>');
        $('.ui-datepicker-next span').replaceWith('<span class="icon-right-open"></span>');
        $('#ui-datepicker-div').append('<div class="header-date-picker"></div>');
    });
    $('.ui-datepicker-next span').click(function(){
        console.log('oi B');
        $('.ui-datepicker-prev span').replaceWith('<span class="icon-left-open"></span>');
        $('.ui-datepicker-next span').replaceWith('<span class="icon-right-open"></span>');
        $('#ui-datepicker-div').append('<div class="header-date-picker"></div>');
    });

    $(document).ready(function(){
        rodape();
        scrollToTop();
        $('.datepicker').focus(function(){
            $('.ui-datepicker-prev span').replaceWith('<span class="icon-left-open"></span>');
            $('.ui-datepicker-next span').replaceWith('<span class="icon-right-open"></span>');
            $('#ui-datepicker-div').append('<div class="header-date-picker"></div>');
        });

        $( "#datepicker" ).datepicker({
            dateFormat: 'dd/mm/yy',
            minDate: 0,
            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
            nextText: '<span class="icon-right-open"></span>',
            prevText: '<span class="icon-left-open"></span>'
        }).attr('readonly','true');


        var now = new Date();
        var outStr = now.getHours();
        if(outStr < 6){
            $('#ui-datepicker-div').addClass('noite');
        }else if(outStr < 12){
            $('#ui-datepicker-div').addClass('manha');
        }else if(outStr < 18){
            $('#ui-datepicker-div').addClass('tarde');
        }else{
            $('#ui-datepicker-div').addClass('noite');
        }


        var nomeTipo = $('#breadcrumbs span span span a').html();
        var nomeServico = $('#breadcrumbs span span span span').html();
        var nomeTipoArchive = $('#breadcrumbs span span span').html();
        if((nomeTipoArchive !== 'Odontologia') && (nomeTipoArchive !== 'Especialidades Médicas') && (nomeTipoArchive !== 'Exames de Imagem') && (nomeTipoArchive !== 'Exames Laboratoriais')){
            nomeTipoArchive = '';
        }

        if(nomeServico || nomeTipoArchive){
            var ajaxUrl = $('#ajx').data('url');
            $.post(ajaxUrl, {
                action:"more_post_ajax",
                competencia: nomeTipo
            }).success(function(posts){
                var postsArray = posts.split('divisor');

                $("select[name=servico]").html(postsArray[0]);
                $("select[name=convenio]").html(postsArray[1]);

                $("#tipoServicos").val(nomeTipo);
                $("select[name=servico]").val(nomeServico);

            });

        }
        setTimeout(function(){
            if(nomeTipoArchive == 'Odontologia'){
                $("#tipoServicos").val(nomeTipoArchive);
                changeTipoServicos();

            }else if(nomeTipoArchive == 'Especialidades Médicas'){
                $("#tipoServicos").val(nomeTipoArchive);
                changeTipoServicos();

            }else if(nomeTipoArchive == 'Exames de Imagem'){
                $("#tipoServicos").val(nomeTipoArchive);
                changeTipoServicos();

            }else if(nomeTipoArchive == 'Exames Laboratoriais'){
                $("#tipoServicos").val(nomeTipoArchive);
                changeTipoServicos();
            }
        }, 2000);

    });

    //SCROOL TO TOP
    function scrollToTop(){
        // browser window scroll (in pixels) after which the "back to top" link is shown
        var offset = 300,
            //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
            offset_opacity = 1200,
            //duration of the top scrolling animation (in ms)
            scroll_top_duration = 700,
            //grab the "back to top" link
            $back_to_top = $('.cd-top');

        //hide or show the "back to top" link
        $(window).scroll(function(){
            ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
            if( $(this).scrollTop() > offset_opacity ) {
                $back_to_top.addClass('cd-fade-out');
            }
        });

        //smooth scroll to top
        $back_to_top.on('click', function(event){
            event.preventDefault();
            $('body,html').animate({
                scrollTop: 0 ,
                }, scroll_top_duration
            );
        });
    }


    //MODAL ORÇAMENTO
    $(".btnagend").on("click", function(e){
        e.preventDefault();
        $(".mask").addClass("active");
    });


    function closeModal(){
        $(".mask").removeClass("active");
    }

    //CLOSE WHEN PRESS ESC
    $(".closeAgend").on("click", function(e){
        closeModal();
    });
    $(".mask").on("click", function(e){
        closeModal();
    });
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            if($(".mask, .mask-news").hasClass("active")){
                closeModal();
            }
        }
    });

    $(window).resize(function() {
        rodape();
    });

    $('#tipoServicos').change(function(){ // When btn is pressed.
        changeTipoServicos();
   });

    function changeTipoServicos(){
        var ajaxUrl = $('#ajx').data('url');
        var competencia = $("select[name=tipo-servicos]").val();

        beforeSend: $("select[name=servico]").html('<option value="0">Carregando Serviços...</option>');
        beforeSend: $("select[name=convenio]").html('<option value="0">Carregando Convênios...</option>');

        $.post(ajaxUrl, {
            action:"more_post_ajax",
            competencia: competencia
        }).success(function(posts){
            var postsArray = posts.split('divisor');

            $("select[name=servico]").html(postsArray[0]);
            $("select[name=convenio]").html(postsArray[1]);
        });
    }


    var divMap = $('#mapa').length;
    if(divMap <= 0){

        // MAPA
        function new_map( $el ) {
            var $markers = $el.find('.marker');
            var args = {
                zoom        : 17,
                center      : new google.maps.LatLng(-19.902655, -43.932895),
                scrollwheel: false,
                mapTypeId   : google.maps.MapTypeId.ROADMAP
            };

            // create map
            var map = new google.maps.Map( $el[0], args);

            // add a markers reference
            map.markers = [];

            // add markers
            $markers.each(function(){
                add_marker( $(this), map );
            });

            // center map
            center_map( map );

            // return
            return map;
        }

        var locations = [
            ['Filial', -19.832556, -43.978343],
            ['Filial', -19.816880, -43.954813],
            ['Filial', -19.817694, -43.955168],
            ['Filial', -19.801556, -44.007894],
            ['Matriz', -19.902655, -43.932895]
        ];
        var infowindow = new google.maps.InfoWindow();
        var marker, i;
        // ADD marker
        function add_marker( $marker, map ) {
            // var
            var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

            // add to array
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                      infowindow.setContent('<p>'+locations[i][0]+'</p>');
                      infowindow.open(map, marker);
                    }
                })(marker, i));
            }

            map.markers.push( marker );
        }

        // Center map
        function center_map( map ) {
            // vars
            var bounds = new google.maps.LatLngBounds();

            // loop through all markers and create bounds
            $.each( map.markers, function( i, marker ){
                var latlng = new google.maps.LatLng( -19.882898, -43.936672 );
                bounds.extend( latlng );
            });

            // only 1 marker?
            if( map.markers.length == 1 ){
                // set center of map
                map.setCenter( bounds.getCenter() );
                map.setZoom( 12 );
            }else{
                // fit to bounds
                map.fitBounds( bounds );
            }
        }


        // global var
        var map = null;
        $(document).ready(function(){
            $('.mapaFooter').each(function(){
                // create map
                map = new_map( $(this) );
            });
        });

        $('.ver-mapaFooter').click(function(e){
            e.preventDefault();
            $('#mapaFooter').addClass('show-mapa');

            $('.footer, body').animate({scrollTop:$('#mapaFooter').offset().top-105}, 900);
        });
    }
})(jQuery);