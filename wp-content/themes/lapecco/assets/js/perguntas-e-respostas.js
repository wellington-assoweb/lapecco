
(function($) {
	$('.nav-perguntas a').click(function(event){
		event.preventDefault();
		var idCurrent = $(this).attr('id');

		$('.nav-perguntas a').removeClass('active-menu');
		$(this).addClass('active-menu');

		$('.lista-perg').removeClass('perg-active');
		$('.'+idCurrent).addClass('perg-active');
	});
})(jQuery);