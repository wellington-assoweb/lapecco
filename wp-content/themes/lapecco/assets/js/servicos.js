(function($) {
	jQuery(document).ready(function($){
		// Slide Odontologia
		var owlClientes = $('.dif .owl-carousel');
		owlClientes.owlCarousel({
			autoplayTimeout: 7000,
			responsiveClass: true,
	        smartSpeed: 750,
	        dots: false,
	        margin: 20,
	        loop: true,
	        nav: true,
	    	// items: 1,
			responsive:{
				0:{
				    items: 1
				},
				600:{
				    items: 2
				},
				991:{
			    	items: 3
				}
			},
			navText: ['<i class="icon-left-open"></i>','<i class="icon-right-open"></i>']
		});
	});
})(jQuery);