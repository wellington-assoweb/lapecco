

(function($) {

	// MAPA
    function new_map( $el ) {
        var $markers = $el.find('.marker');
        var args = {
            zoom        : 17,
            center      : new google.maps.LatLng(-19.902655, -43.932895),
            scrollwheel: false,
            mapTypeId   : google.maps.MapTypeId.ROADMAP
        };

        // create map
        var map = new google.maps.Map( $el[0], args);

        // add a markers reference
        map.markers = [];

        // add markers
        $markers.each(function(){
            add_marker( $(this), map );
        });

        // center map
        center_map( map );

        // return
        return map;
    }

    var locations = [
        ['Filial', -19.832556, -43.978343],
        ['Filial', -19.816880, -43.954813],
        ['Filial', -19.817694, -43.955168],
        ['Filial', -19.801556, -44.007894],
        ['Matriz', -19.902655, -43.932895]
    ];
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    // ADD marker
    function add_marker( $marker, map ) {
        // var
        var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

        // add to array
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                  infowindow.setContent('<p>'+locations[i][0]+'</p>');
                  infowindow.open(map, marker);
                }
            })(marker, i));
        }

        map.markers.push( marker );
    }

    // Center map
    function center_map( map ) {
        // vars
        var bounds = new google.maps.LatLngBounds();

        // loop through all markers and create bounds
        $.each( map.markers, function( i, marker ){
            var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
            bounds.extend( latlng );
        });

        // only 1 marker?
        if( map.markers.length == 1 ){
            // set center of map
            map.setCenter( bounds.getCenter() );
            map.setZoom( 12 );
        }else{
            // fit to bounds
            map.fitBounds( bounds );
        }
    }


    // global var
    var map = null;
    $(document).ready(function(){
        $('.mapa').each(function(){
            // create map
            map = new_map( $(this) );
        });
    });


    doc = $('html, body');
    $('.ver-mapa').click(function(){
        $('#mapa').toggleClass('show-mapa');
        $('.ver-mapa').toggleClass('active');

        $('html,body').animate({scrollTop:$('#mapa').offset().top-105}, 900);
    });


})(jQuery);
