(function($) {
	$('.abas li').click(function(){
		$('.bx-item').removeClass('active');
		$('.abas li').removeClass('active');
		id = $(this).data('id');

		$(this).addClass('active');
		$('#'+id).addClass('active');
	});


	$(document).ready(function(){
		setTimeout(function(){
			heightConven = $('.bx-cont').height();
			$('.bx-agendar').css('min-height',heightConven+100);
		}, 2000);

		function loadAgend(){
			var nomeTipo = $('.abas li.active').html();

	        var ajaxUrl = $('#ajx').data('url');
	        $.post(ajaxUrl, {
	            action:"more_post_ajax",
	            competencia: nomeTipo
	        }).success(function(posts){
	            var postsArray = posts.split('divisor');

	            $("select[name=servico]").html(postsArray[0]);
	            $("select[name=convenio]").html(postsArray[1]);

	            $("#tipoServicos").val(nomeTipo);
	        });
	    }
	    loadAgend();
		$('.btnagend a').click(function(){
			loadAgend();
		});
		$('.abas li').click(function(){
			loadAgend();
		});
		$('.btnagend').click(function(e){
			$("select[name=convenio]").val('');
			loadAgend();
			nomeConv = $(this).data('conv');
			setTimeout(function(){
			  $("select[name=convenio]").val(nomeConv);
			}, 2000);

		});
	});
})(jQuery);