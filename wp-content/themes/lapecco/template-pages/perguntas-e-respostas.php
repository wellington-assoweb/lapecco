<?php get_header(); /* Template name: Perguntas e Respostas */ ?>

<section class="head-title bg-header-<?php echo $post->ID; ?>">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-name">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                    } ?>
                    <h1 class="title-princ"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="perg-e-res">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-sm-12 col-md-3 ctrl-nav-per">
                    <div class="nav-perguntas">
                        <nav>
                            <ul>
                                <li class="item">
                                    <a href="script:void(0)" id="exames" class="titulo-perguntas-frequentes active-menu">
                                        </span>Exames
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="script:void(0)" id="exames_de_urina" class="titulo-perguntas-frequentes">
                                        </span>Exames de Urina
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="script:void(0)" id="exames_de_fezes" class="titulo-perguntas-frequentes">
                                        </span>Exames de Fezes
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="script:void(0)" id="exames_de_imagem" class="titulo-perguntas-frequentes">
                                        </span>Exames de imagem
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="script:void(0)" id="acesso_resultados" class="titulo-perguntas-frequentes">
                                        </span>Acesso Resultado
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <div class="col-sm-12 col-md-9">
                    <form>
                        <div class="lista-perg exames perg-active">
                            <h3 class="sub-title-perg">Exames</h3>
                            <?php $count = 1; ?>
                            <?php while( have_rows('exames') ): the_row();
                                // vars
                                $pergunta = get_sub_field('pergunta');
                                $resposta = get_sub_field('resposta');
                            ?>
                            <input type="radio" id="radio-option-<?php echo $count; ?>" name="accordion-radios">
                            <div class="baffle">
                                <div class="perguntas-frequentes">
                                    <label for="radio-option-<?php echo $count; ?>" title="Ver resposta"><?php echo $pergunta ?></label>
                                </div>
                                <div class="respostas-frequentes">
                                    <?php echo $resposta ?>
                                </div>
                            </div>
                            <?php $count++; endwhile; ?>
                        </div>

                        <div class="lista-perg exames_de_urina">
                            <h3 class="sub-title-perg">Exames de Urina</h3>
                            <?php while( have_rows('exames_de_urina') ): the_row();
                                // vars
                                $pergunta = get_sub_field('pergunta');
                                $resposta = get_sub_field('resposta');
                            ?>
                            <input type="radio" id="radio-option-<?php echo $count; ?>" name="accordion-radios">
                            <div class="baffle">
                                <div class="perguntas-frequentes">
                                    <label for="radio-option-<?php echo $count; ?>" title="Ver resposta"><?php echo $pergunta ?></label>
                                </div>
                                <div class="respostas-frequentes">
                                    <?php echo $resposta ?>
                                </div>
                            </div>
                            <?php $count++; endwhile; ?>
                        </div>

                        <div class="lista-perg exames_de_fezes">
                            <h3 class="sub-title-perg">Exames de Fezes</h3>
                            <?php while( have_rows('exames_de_fezes') ): the_row();
                                // vars
                                $pergunta = get_sub_field('pergunta');
                                $resposta = get_sub_field('resposta');
                            ?>
                            <input type="radio" id="radio-option-<?php echo $count; ?>" name="accordion-radios">
                            <div class="baffle">
                                <div class="perguntas-frequentes">
                                    <label for="radio-option-<?php echo $count; ?>" title="Ver resposta"><?php echo $pergunta ?></label>
                                </div>
                                <div class="respostas-frequentes">
                                    <?php echo $resposta ?>
                                </div>
                            </div>
                            <?php $count++; endwhile; ?>
                        </div>

                        <div class="lista-perg exames_de_imagem">
                            <h3 class="sub-title-perg">Exames de imagem</h3>
                            <?php while( have_rows('exames_de_imagem') ): the_row();
                                // vars
                                $pergunta = get_sub_field('pergunta');
                                $resposta = get_sub_field('resposta');
                            ?>
                            <input type="radio" id="radio-option-<?php echo $count; ?>" name="accordion-radios">
                            <div class="baffle">
                                <div class="perguntas-frequentes">
                                    <label for="radio-option-<?php echo $count; ?>" title="Ver resposta"><?php echo $pergunta ?></label>
                                </div>
                                <div class="respostas-frequentes">
                                    <?php echo $resposta ?>
                                </div>
                            </div>
                            <?php $count++; endwhile; ?>
                        </div>

                        <div class="lista-perg acesso_resultados">
                            <h3 class="sub-title-perg">Acesso Resultado</h3>
                            <?php while( have_rows('acesso_resultados') ): the_row();
                                // vars
                                $pergunta = get_sub_field('pergunta');
                                $resposta = get_sub_field('resposta');
                            ?>
                            <input type="radio" id="radio-option-<?php echo $count; ?>" name="accordion-radios">
                            <div class="baffle">
                                <div class="perguntas-frequentes">
                                    <label for="radio-option-<?php echo $count; ?>" title="Ver resposta"><?php echo $pergunta ?></label>
                                </div>
                                <div class="respostas-frequentes">
                                    <?php echo $resposta ?>
                                </div>
                            </div>
                            <?php $count++; endwhile; ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>