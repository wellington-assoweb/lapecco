<?php get_header(); /* Template name: Serviços */ ?>

<section class="head-title bg-header-<?php echo $post->ID; ?>">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-name">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                    } ?>
                    <h1 class="title-princ"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="servicos">
    <div class="my-container menor">
        <div class="row">
            <?php
                while ( have_rows('todos_os_servicos') ) : the_row();
                    $imagem = get_sub_field('imagem');
                    $nome = get_sub_field('nome');
                    $link_para_o_servico = get_sub_field('link_para_o_servico');
            ?>
                        <div class="item">
                            <img src="<?php echo $imagem['url'] ?>" alt="<?php echo $imagem['alt'] ?>" title="<?php echo $imagem['title'] ?>">
                            <h3><?php echo $nome; ?></h3>
                            <div class="btn gradient center">
                                <a href="<?php echo $link_para_o_servico; ?>"><span>Saiba Mais</span></a>
                            </div>
                        </div>
            <?php endwhile; wp_reset_query(); ?>
        </div>
    </div>
</section>
<?php include(TEMPLATEPATH . '/template-parts/melhores-prof.php'); ?>
<?php get_footer(); ?>