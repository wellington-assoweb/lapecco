<?php get_header(); /* Template name: Concursos */ ?>

<section class="head-title bg-header-<?php echo $post->ID; ?>">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-name">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                    } ?>
                    <h1 class="title-princ"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pac-med">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="title"><?php echo get_field('titulo_paciente'); ?></h2>
                <?php echo get_field('conteudo_paciente'); ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <h2 class="title"><?php echo get_field('titulo_medico'); ?></h2>
                <?php echo get_field('titulo_conteudo'); ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>