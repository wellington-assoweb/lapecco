<?php get_header(); /* Template name: Contato */

?>
<section class="head-title bg-header-<?php echo $post->ID; ?>">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-name">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                    } ?>
                    <h1 class="title-princ"><?php the_title(); ?></h1>
                </div>
                <div class="bx-form">
	                <?php echo do_shortcode('[contact-form-7 id="4" title="Formulário de contato"]'); ?>
                </div>
                <div class="bx-cont">
	          		<h2>Mais do que uma clínica de saúde, somos um grupo completo.</h2>

                    <b>Endereço:</b>
                    <span>Rua Jacuí, 1204 - Bairro Floresta - Belo Horizonte - Minas Gerais</span>
                    <div><a class="link-fili" href="<?php echo SITEURL ?>/filiais-para-laboratorios">Filiais do Laboratório</a></div>

                    <b>Telefone:</b>
                    <span>(31) 3449-8500</span>

                    <b>E-mail:</b>
                    <span>faleconosco@nucleolapecco.com.br</span>

                    <b>Dúvidas:</b>
                    <a href="<?php echo SITEURL ?>/perguntas-e-respostas"><span>Perguntas e Respostas</span></a>
                </div>

                <img class="ver-mapa active" src="<?php echo THEMEURL ?>/assets/img/ver-mapa.png" alt="">
                <img class="ver-mapa" src="<?php echo THEMEURL ?>/assets/img/fechar-mapa.png" alt="">
            </div>
        </div>
    </div>
</section>



<?php $location = get_field('mapa');
if( !empty($location) ): ?>
    <div class="mapa" id="mapa">
        <div class="marker" id="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
    </div>
<?php endif; ?>


<?php get_footer(); ?>




