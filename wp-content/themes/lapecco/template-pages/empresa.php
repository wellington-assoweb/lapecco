<?php get_header(); /* Template name: Empresa */ ?>

<section class="head-title big">
	<div class="bg-title"  style="background-image:url('<?php echo THEMEURL ?>/assets/img/a-empresa-header.jpg')"></div>
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="page-name">
					<?php if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
					} ?>
					<h1 class="title-princ"><?php the_title(); ?></h1>
				</div>
				<div class="desc">
					<h2><?php echo get_field('frase_titulo_do_cabecalho'); ?></h2>
					<?php echo get_field('conteudo_do_cabecalho'); ?>
					<div class="btn gradient center">
						<a href="<?php echo get_field('link_do_botao_do_cabecalho'); ?>"><span><?php echo get_field('texto_do_botao_do_cabecalho'); ?></span></a>
					</div>
				</div>
			</div>
			<div class="lapecco-nome">
				<img src="<?php echo THEMEURL ?>/assets/img/lapecco-nome-38.png" alt="Logo Lapecco" title="Logo Lapecco">
			</div>
		</div>
	</div>
</section>

<section class="o-q-faz">
	<div class="my-container">
		<div class="row">
			<h2 class="title">O que fazemos</h2>

			<?php
				while(have_rows('itens_fazemos')): the_row();
			?>
			<div class="bx-item">
				<div class="item">
					<span><?php echo get_sub_field('nome'); ?></span>
					<div class="btn gradient center">
						<a href="<?php echo get_sub_field('link_do_botao'); ?>"><span><?php echo get_sub_field('texto_do_botao'); ?></span></a>
					</div>
				</div>
			</div>
			<?php
				endwhile;
			?>
		</div>
	</div>
</section>

<section class="nossa-est">
	<div class="my-container menor">
		<div class="row">
			<div class="col-xs-12 col-md-offset-4 col-md-8">
				<h2 class="title"><?php echo get_field('titulo_estrutura'); ?></h2>
			</div>
			<div class="col-xs-12 col-md-6">
				<?php
					$argsHome = array(
				        'post__in' => array( 2 ),
				        'post_type' => 'page'
				    );
				    $queryHome = new WP_Query( $argsHome );

					if($queryHome->have_posts()) : while( $queryHome->have_posts() ) {
				        $queryHome->the_post();
						$imgSelo = get_field('imagem_selo');
						$img_srcset_a2 = wp_get_attachment_image_srcset($imgSelo['ID'], 'full' );
					?>
					<img src="<?php echo $imgSelo['url'];?>" srcset="<?php echo esc_attr( $img_srcset_a2 ); ?>" sizes="(max-width: 80em) 100vw, 98px" alt="<?php echo $imgSelo['alt'];?>">
					<div class="cont">
						<h2><?php echo get_field('titulo_selo'); ?></h2>
						<p><?php echo get_field('conteudo_selo'); ?></p>
					</div>
				<?php } endif; wp_reset_query(); ?>

				<div class="btn gradient center">
					<a href="<?php echo get_field('link_do_botao_estrutura'); ?>"><span><?php echo get_field('texto_do_botao_estrutura'); ?></span></a>
				</div>

			</div>
			<div class="col-xs-12 col-md-6">
				<iframe width="100%" height="515"
				src="https://www.youtube.com/embed/<?php echo get_field('codigo_do_video'); ?>?autoplay=0">
				</iframe>
			</div>
		</div>
	</div>
</section>

<section class="bx-gray">
	<div class="my-container menor">
		<div class="row">
			<div class="col-xs-12 col-md-5">
				<div class="cont">
					<h2><?php echo get_field('titulo_sessao_slide'); ?></h2>
					<?php echo get_field('conteudo_sessao_slide'); ?>
					<div class="btn gradient center btn-desk">
						<a href="<?php echo get_field('link_do_botao_do_slide'); ?>"><span><?php if(get_field('texto_do_botao_do_slide')){ echo get_field('texto_do_botao_do_slide'); }else{ echo 'Conheça nossos serviços'; } ?></span></a>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-7">
				<div class="owl-carousel">
					<?php
						while ( have_rows('imagens_sessão_slide') ) : the_row();
							$imagem = get_sub_field('imagem');
					?>
						<div class="item">
							<div class="bx-img slide-emp-<?php echo $imagem['id']; ?>"></div>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
			<div class="btn gradient center btn-resp">
				<a href="#"><span>Conheça as especialidades</span></a>
			</div>
		</div>
	</div>
</section>


<section class="diret">
	<div class="my-container menor">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="title"><?php echo get_field('titulo_diretoria'); ?></h2>
			</div>
			<?php
				while ( have_rows('diretores') ) : the_row();
					$imagem = get_sub_field('imagem');
					$nome = get_sub_field('nome');
					$conteudo = get_sub_field('conteudo');
			?>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="bx-img">
						<img src="<?php echo $imagem['url'] ?>" alt="<?php echo $imagem['alt'] ?>" title="<?php echo $imagem['title'] ?>">
					</div>
					<div class="cont">
						<h3><?php echo $nome; ?></h3>
						<p><?php echo $conteudo; ?></p>
					</div>
				</div>
			<?php
				endwhile;
			?>
		</div>
	</div>
</section>


<?php get_footer(); ?>