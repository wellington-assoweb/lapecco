<?php get_header(); /* Template name: Resultados Online */ ?>

<section class="head-title bg-header-<?php echo $post->ID; ?>">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-name">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                    } ?>
                    <h1 class="title-princ"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pac-med">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h2 class="title"><?php echo get_field('titulo_paciente'); ?></h2>
                <?php echo get_field('conteudo_paciente'); ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <h2 class="title"><?php echo get_field('titulo_medico'); ?></h2>
                <?php echo get_field('titulo_conteudo'); ?>
            </div>
        </div>
    </div>
</section>

<section class="box-form">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-md-offset-3 col-md-6">
                <h2 class="title">Preencha os campos abaixo:</h2>
                <!-- <form action="">
                    <div class="campo">
                        <input type="text" placeholder="Chave">
                    </div>
                    <div class="campo">
                        <input type="text" placeholder="Senha">
                    </div>
                    <div class="campo btn gradient">
                        <input type="submit" value="Acessar">
                    </div>
                </form> -->
                <form id="frmAutenticacao" name="frmAutenticacao"  method="post" onSubmit="return valida()"  action="https://www.resultados.com.br/index.aspx?origem=lapecco" target="blank">
                    <input type="hidden" name="frame" value="S">
                    <input type="hidden" name="redir" value="https://www.resultados.com.br/clientes/lapecco/lapecco.aspx">
                    <div class="campo">
                        <input name="identificacao"  placeholder="Chave" id="identificacao" class="required" type="text" maxlength="20"><br/>
                    </div>
                    <div class="campo">
                        <input type="password"  placeholder="Senha" class="required email" id="subs-email" name="senha" id="senha" maxlength="8">
                    </div>
                    <div class="campo btn gradient">
                        <!-- <a id="submeter" title="" href=""></a> -->
                        <input type="submit" id="inpu" name="inpu"  value="    Acessar    " name="Ok" />
                    </div>
                </form>

                <div class="aces-perg">Ficou alguma dúvida, clique no menu abaixo</div>
            </div>
        </div>
    </div>
</section>


<section class="perg-e-res">
    <div class="my-container">
        <div class="row">
            <div class="col-sm-12 col-md-3 ctrl-nav-per">
                <div class="nav-perguntas">
                    <nav>
                        <ul>
                            <li class="item">
                                <a href="script:void(0)" id="acesso_resultados" class="titulo-perguntas-frequentes active-menu">
                                    </span>Acesso Resultado
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <?php
                $argsServPage = array(
                    'post__in' => array( 889 ),
                    'post_type' => 'page'
                );
                $queryServPage = new WP_Query( $argsServPage );

                $queryServPage->the_post();
            ?>
            <div class="col-sm-12 col-md-9">
                <form>
                    <div class="lista-perg acesso_resultados perg-active">
                        <h3 class="sub-title-perg">Acesso Resultado</h3>
                        <?php while( have_rows('acesso_resultados') ): the_row();
                            // vars
                            $pergunta = get_sub_field('pergunta');
                            $resposta = get_sub_field('resposta');
                        ?>
                        <input type="radio" id="radio-option-<?php echo $count; ?>" name="accordion-radios">
                        <div class="baffle">
                            <div class="perguntas-frequentes">
                                <label for="radio-option-<?php echo $count; ?>" title="Ver resposta"><?php echo $pergunta ?></label>
                            </div>
                            <div class="respostas-frequentes">
                                <?php echo $resposta ?>
                            </div>
                        </div>
                        <?php $count++; endwhile; ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="cons slide">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="title">Serviços de Odontologia em BH</h3>
            </div>
            <div class="col-xs-12">
                <div class="owl-carousel">
                    <?php
                        $countDelay = 0.0;
                        $argsOdont = array(
                            'post_type'         => 'odontologia',
                            'posts_per_page'    => -1,
                            'post__not_in'      => array($post->ID)
                        );
                        $odont = new WP_Query( $argsOdont );

                        if($odont->have_posts()) : while( $odont->have_posts() ) {
                            $odont->the_post();
                            $imagem_da_chamada_do_servico = get_field('imagem_da_chamada_do_servico');
                    ?>
                            <div class="item wow fadeInUp" data-wow-duration="1s" data-wow-delay="<?php echo $countDelay ?>s">
                                <a href="<?php the_permalink(); ?>"></a>
                                <div class="bx-img">
                                    <img src="<?php echo $imagem_da_chamada_do_servico['url'] ?>" alt="">
                                </div>
                                <span><?php the_title(); ?></span>
                            </div>
                    <?php
                            $countDelay = $countDelay + 0.2;
                        }
                        endif;
                        wp_reset_query();
                    ?>
                </div>
                <div class="btn gradient center">
                    <a href="<?php echo SITEURL ?>/odontologia"><span>Todos os Serviços</span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>