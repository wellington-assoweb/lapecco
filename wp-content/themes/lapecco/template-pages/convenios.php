<?php get_header(); /* Template name: Convênio */ ?>

<section class="head-title bg-header-<?php echo $post->ID; ?>">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-name">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                    } ?>
                    <h1 class="title-princ"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="conven">
    <div class="my-container">
        <div class="row">
			<div class="col-xs-12 col-md-3">
				<div class="bx-cont">
					<h2><?php echo get_field('titulo_do_convenio'); ?></h2>
					<?php echo get_field('conteudo_do_convenio'); ?>
				</div>
			</div>
			<div class="col-xs-12 col-md-9 bx-agendar">
				<ul class="abas">
					<li data-id="odon" class="active">Odontologia</li>
					<li data-id="exLab">Exames Laboratoriais</li>
					<li data-id="exImg">Exames de Imagem</li>
					<li data-id="consul">Especialidades Médicas</li>
				</ul>

				<?php
					$argsServPage = array(
				        'post__in' => array( 419 ),
				        'post_type' => 'page'
				    );
				    $queryServPage = new WP_Query( $argsServPage );

				    $queryServPage->the_post();
		        ?>
				<div class="ctrl-itens">
					<div id="odon" class="bx-item active">
						<?php
							$convenio_odontologia = get_field('convenio_odontologia');
							foreach( $convenio_odontologia as $p):
						?>
							<div class="item">
								<h3><?php echo get_the_title($p->ID); ?></h3>
								<div class="btn gradient center">
									<a href="#" class="btnagend" data-conv="<?php echo get_the_title($p->ID); ?>">
										<span>Agendar Consulta</span>
									</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
					<div id="exLab" class="bx-item">
						<?php
							$convenio_exames_laboratoriais = get_field('convenio_exames_laboratoriais');
							foreach( $convenio_exames_laboratoriais as $p):
						?>
							<div class="item">
								<h3><?php echo get_the_title($p->ID); ?></h3>
								<div class="btn gradient center">
									<a href="#" class="btnagend" data-conv="<?php echo get_the_title($p->ID); ?>">
										<span>Agendar Exame</span>
									</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
					<div id="exImg" class="bx-item">
						<?php
							$convenio_exames_de_imagem = get_field('convenio_exames_de_imagem');
							foreach( $convenio_exames_de_imagem as $p):
						?>
							<div class="item">
								<h3><?php echo get_the_title($p->ID); ?></h3>
								<div class="btn gradient center">
									<a href="#" class="btnagend" data-conv="<?php echo get_the_title($p->ID); ?>">
										<span>Agendar Exame</span>
									</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
					<div id="consul" class="bx-item">
						<?php
							$convenio_especialidade_medica = get_field('convenio_especialidade_medica');
							foreach( $convenio_especialidade_medica as $p):
						?>
							<div class="item">
								<h3><?php echo get_the_title($p->ID); ?></h3>
								<div class="btn gradient center">
									<a href="#" class="btnagend" data-conv="<?php echo get_the_title($p->ID); ?>">
										<span>Agendar Consulta</span>
									</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<?php get_footer(); ?>