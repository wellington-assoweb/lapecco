<?php get_header(); ?>

<section class="head-title bg-header-<?php echo $post->ID; ?>">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="page-name">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                    } ?>
                    <h1 class="title-princ"><?php the_title(); ?></h1>
                </div>
                <?php
                    $icone_cabecalho_do_servico = get_field('icone_cabecalho_do_servico');
                ?>

                <img class="icone" src="<?php echo $icone_cabecalho_do_servico['url'] ?>" alt="<?php echo $icone_cabecalho_do_servico['alt'] ?>" >
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="bx-cont">
                    <h2 class="title-custom"><?php if(get_field('titulo_box_branco')){ echo get_field('titulo_box_branco'); }else{ echo 'Mais do que uma clínica de saúde, somos um grupo completo.'; } ?></h2>
                    <?php echo get_field('conteudo_box_branco'); ?>
                    <div class="btn gradient center">
                        <a href="tel:+553134498500"><span><?php if(get_field('texto_do_botao_sessao_um')){ echo get_field('texto_do_botao_sessao_um'); }else{echo 'Ligue para nós'; } ?></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="desc-princ">
    <div class="my-container">
        <div class="row">
            <div class="bx-cont">
                <h2 class="title"><?php if(get_field('titulo_sessao_um')){ echo get_field('titulo_sessao_um'); }else{ echo 'Lorem ipsum dolor sit amet, consectetur adipisicing'; }?></h2>
                <h3 class="subtitle"><?php if(get_field('subtitulo_sessão_um')){ echo get_field('subtitulo_sessão_um'); }else{ echo 'Sint, placeat, quas. Eos, vitae tempora quisquam accusanti.'; }?></h3>
                <?php if(get_field('conteudo_sessao_um')){ echo get_field('conteudo_sessao_um'); }else{ echo '<p>Grandes empresas se baseiam em grandes virtudes para crescer: seriedade, qualidade e profissionalismo. Há mais de 35 anos, uma empresa se instalou no coração da rua Jacuí, o Laboratório Lapecco. Desde então a região leste e nordeste de Belo Horizonte contava com o valioso trabalho do Lapecco em exames laboratoriais à serviço do bem estar da comunidade.</p><p>Durante décadas o Lapecco vem realizando com competência trabalhos na área da saúde com profissionalismo reconhecido pelo público e por nossos clientes. Esta credibilidade e reconhecimento foram fundamentais para que o Lapecco, hoje, seja uma grande empresa referência em serviços de qualidade, em toda grande BH.</p>'; } ?>


                <div class="btn gradient">
                    <a href="tel:+553134498500"><span><?php if(get_field('texto_do_botao_sessao_um')){ echo get_field('texto_do_botao_sessao_um'); }else{echo 'Ligue para nós'; } ?></span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
    $argsServPage = array(
        'post__in' => array( 419 ),
        'post_type' => 'page'
    );
    $queryServPage = new WP_Query( $argsServPage );

    $queryServPage->the_post();
    $fundo_slide_concurso = get_field('fundo_slide_concurso');
?>
    <section class="slide-full-wide bg-slide-<?php echo $fundo_slide_concurso['ID']; ?>">
        <div class="my-container">
            <div class="row">
                <div class="bx-cont">
                    <h3 class="title-custom"><?php echo get_field('titulo_slide_concurso'); ?></h3>
                    <?php echo get_field('conteudo_slide_concurso'); ?>
                </div>
            </div>
        </div>
    </section>
<?php wp_reset_query();

    include(TEMPLATEPATH . '/template-parts/melhores-prof.php');
    include(TEMPLATEPATH . '/template-parts/slide-consultas.php');
    get_footer();
?>