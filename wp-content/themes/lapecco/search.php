<?php get_header();
	include(TEMPLATEPATH . '/template-parts/header-blog.php');
?>
<section class="box-blog">
    <div class="posts">
        <div class="my-container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-lg-9">
                	<div class="row">
                		<div class="col-xs-12">
							<?php
							 	$num = $wp_query->post_count;
			                    if($num <= 0): ?>
			                        <h1 class="title-princ-blog">Não encontramos nenhum resultado para: <i>"<?php echo get_search_query(); ?>"</i></h1>
			                    <?php elseif($num == 1): ?>
			                        <h1 class="title-princ-blog">Encontramos 1 resultado para: <i>"<?php echo get_search_query(); ?>"</i></h1>
			                    <?php elseif($num > 1): ?>
			                        <h1 class="title-princ-blog">Encontramos <?php echo $num; ?> resultados para: <i>"<?php echo get_search_query(); ?>"</i></h1>
			                    <?php endif; ?>
							<div class="busca bx-search">
								<form id="formheader" method="get" action="<?php echo SITEURL; ?>/">
									<input class="sb-search-input" type="text" value="" name="s" id="s" required placeholder="Digite sua busca">
									<input class="sb-search-submit" type="submit" value="Buscar">
									<i class="icon-search"></i>
								</form>
							</div>
                		</div>

						<?php
						if (have_posts()):
							while(have_posts()):
								the_post(); ?>
								<div class="col-xs-12 col-sm-12 col-md-6">
                                    <article class="one-article" itemscope itemtype="http://schema.org/NewsArticle">
                                        <div class="categ">
                                            <?php
                                            $categoria = get_the_category();
                                            foreach($categoria as $category) {
                                                $output = '<a class="nome-categoria" href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "Ver todos os posts de %s" ), $category->name ) ) . '"><span>'.$category->cat_name.'</span></a>';
                                            }
                                            echo $output; ?>
                                        </div>
                                        <a href="<?php the_permalink(); ?>">
                                            <figure itemprop="image" class="post-ult-<?php echo $id; ?>">
                                                <figcaption class="hidden">
                                                    <h4><?php the_title(); ?></h4>
                                                </figcaption>
                                            </figure>
                                        </a>
                                        <div class="description">
                                            <h3 class="title-post" itemprop="headline">
                                                <a href="<?php the_permalink(); ?>"><?php the_title_limit($post->Id, 55); ?></a>
                                            </h3>
                                            <div class="date">
                                                <data itemprop="datePublished" value="<?php the_time('j \d\e F \d\e Y'); ?>"><?php the_time('j \d\e F \d\e Y'); ?></data>
                                            </div>
                                            <div class="hidden" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                                                <span itemprop="name">Por <?php the_author_posts_link(); ?></span>
                                            </div>
                                            <div class="desc-post" itemprop="description">
                                                <p><?php the_content_limit(185); ?></p>
                                            </div>
                                            <div class="btn gradient">
                                                <a href="<?php the_permalink(); ?>"><span>Continuar Lendo</span></a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
						  	<?php endwhile; ?>
	  					</div><!-- ./row -->
		  				<?php else: ?>
	  						</div><!-- ./row -->
	  						<div class="row">
		  						<div class="col-xs-12">
	  								<h3 class="title-outros-art">Confira outros artigos</h3>
		  						</div>
			  					<?php
			  						$postsSearch = $_SESSION["postsSearch"];
				            		while ( $postsSearch->have_posts() ) : $postsSearch->the_post(); ?>
										<div class="col-xs-12 col-sm-12 col-md-6">
		                                    <article class="one-article" itemscope itemtype="http://schema.org/NewsArticle">
		                                        <div class="categ">
		                                            <?php
		                                            $categoria = get_the_category();
		                                            foreach($categoria as $category) {
		                                                $output = '<a class="nome-categoria" href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "Ver todos os posts de %s" ), $category->name ) ) . '"><span>'.$category->cat_name.'</span></a>';
		                                            }
		                                            echo $output; ?>
		                                        </div>
		                                        <a href="<?php the_permalink(); ?>">
		                                            <figure itemprop="image" class="post-ult-<?php echo $id; ?>">
		                                                <figcaption class="hidden">
		                                                    <h4><?php the_title(); ?></h4>
		                                                </figcaption>
		                                            </figure>
		                                        </a>
		                                        <div class="description">
		                                            <h3 class="title-post" itemprop="headline">
		                                                <a href="<?php the_permalink(); ?>"><?php the_title_limit($post->Id, 55); ?></a>
		                                            </h3>
		                                            <div class="date">
		                                                <data itemprop="datePublished" value="<?php the_time('j \d\e F \d\e Y'); ?>"><?php the_time('j \d\e F \d\e Y'); ?></data>
		                                            </div>
		                                            <div class="hidden" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
		                                                <span itemprop="name">Por <?php the_author_posts_link(); ?></span>
		                                            </div>
		                                            <div class="desc-post" itemprop="description">
		                                                <p><?php the_content_limit(185); ?></p>
		                                            </div>
		                                            <div class="btn gradient">
		                                                <a href="<?php the_permalink(); ?>"><span>Continuar Lendo</span></a>
		                                            </div>
		                                        </div>
		                                    </article>
		                                </div>
				            			<?php
				            		endwhile;
									wp_reset_postdata();
									wp_reset_query();
								?>
		  					</div>
	  				<?php endif; ?>
	  			</div>
	            <div class="col-xs-12 col-sm-4 col-lg-3">
                    <?php include(TEMPLATEPATH . '/template-parts/sidebar-blog.php'); ?>
                </div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>