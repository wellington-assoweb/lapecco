<?php

/* Insert ACF Google maps API*/
add_filter('acf/settings/google_api_key', function () {
    return 'AIzaSyAzGJyE5c7gxHOnM2O7eDTHemt0X56irmU';
});

/*=========================================================================================
LOAD SCRIPTS AND STYLES
=========================================================================================*/
add_action('wp_enqueue_scripts', 'sample_scripts');
function sample_scripts() {
  //PEGA O DIRETÓRIO DEFAULT DOS ARQUIVOS PARA CONCATENAR
  define(CSS_PATH, THEMEURL.'/assets/css/');
  define(JS_PATH, THEMEURL.'/assets/js/'); /* /assets/js/ para arquivos base ou /assets/js/min/ para diretório de scripts minificados */
  define(JS_PREFIX, '.js'); // Altere aqui a extensão para trabalhar com os scripst originais ou minificados

  wp_enqueue_script('jquery');

  wp_register_style('animate-css', CSS_PATH.'animate.min.css', null, null, 'all' );
  wp_register_style('fontello-css', CSS_PATH.'fontello.css', null, null, 'all' );
  wp_register_style('menu-resp-css', CSS_PATH.'menu-resp.css', null, null, 'all' );



  //Declaração de scripts
  wp_register_script( 'googlemaps-api', '//maps.googleapis.com/maps/api/js?v=3&key=AIzaSyAzGJyE5c7gxHOnM2O7eDTHemt0X56irmU&signed_in=true', null, null, false);
  wp_register_script('bootstrap_js', JS_PATH.'bootstrap'.JS_PREFIX.'', null, null, true );
   wp_register_script('menu-js', JS_PATH.'menu.js', null, null, true );
  wp_register_script('mailchimp','//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js', null, null, true );
  wp_register_script('smooth-scroll', JS_PATH.'smooth-scroll'.JS_PREFIX.'', null, null, true );
  wp_register_script('scripts', JS_PATH.'scripts'.JS_PREFIX.'', null, null, true );

  wp_register_script('jquery-ui-js', JS_PATH.'jquery-ui.min'.JS_PREFIX.'', null, null, true );

  wp_enqueue_script('bootstrap_js');
  //wp_enqueue_script('mailchimp');
  wp_enqueue_script('menu-js');
  wp_enqueue_script('smooth-scroll');
  wp_enqueue_style('fontello-css');
  wp_enqueue_style('menu-resp-css');




  /*FIM MENU*/
  if(is_front_page()){
    wp_enqueue_style('animate-css');
    wp_register_style('front-page-css', CSS_PATH.'front-page.css', null, null, 'all' );
    wp_enqueue_style('front-page-css');

    wp_register_script('wow-js', JS_PATH.'wow.js', null, null, true );
    wp_enqueue_script('wow-js');
    wp_register_script('owl.carousel-js', JS_PATH.'owl.carousel.min'.JS_PREFIX.'', null, null, true );
    wp_enqueue_script('owl.carousel-js');
    wp_register_script('front-page-js', JS_PATH.'front-page'.JS_PREFIX.'', null, null, true );
    wp_enqueue_script('front-page-js');


  }elseif(is_page()){

    if(is_page(392)){ // EMPRESA
      wp_register_style('empresa-css', CSS_PATH.'empresa.css', null, null, 'all' );
      wp_enqueue_style('empresa-css');

      wp_register_script('owl.carousel-js', JS_PATH.'owl.carousel.min'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('owl.carousel-js');
      wp_register_script('empresa-js', JS_PATH.'empresa'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('empresa-js');
    }
    else if(is_page(458)){ // Contato
      wp_register_style('contato-css', CSS_PATH.'contato.css', null, null, 'all' );
      wp_enqueue_style('contato-css');
      wp_register_script('contato-js', JS_PATH.'contato'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('contato-js');
    }
    else if(is_page(388)){ // Convênios
      wp_register_style('convenios-css', CSS_PATH.'convenios.css', null, null, 'all' );
      wp_enqueue_style('convenios-css');

      wp_register_script('convenios-js', JS_PATH.'convenios'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('convenios-js');
    }
    else if(is_page(419)){ // Serviços
      wp_register_style('servicos-archive-css', CSS_PATH.'servicos-archive.css', null, null, 'all' );
      wp_enqueue_style('servicos-archive-css');
    }
    else if(is_page(849)){ // Resultados online
      wp_register_style('resultados-online-css', CSS_PATH.'resultados-online.css', null, null, 'all' );
      wp_enqueue_style('resultados-online-css');
      wp_register_script('owl.carousel-js', JS_PATH.'owl.carousel.min'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('owl.carousel-js');
      wp_register_script('perguntas-e-respostas-js', JS_PATH.'perguntas-e-respostas'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('perguntas-e-respostas-js');
      wp_register_script('resultados-online-js', JS_PATH.'resultados-online'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('resultados-online-js');
    }
    else if(is_page(889)){ // Perguntas e Respostas
      wp_register_style('perguntas-e-respostas-css', CSS_PATH.'perguntas-e-respostas.css', null, null, 'all' );
      wp_enqueue_style('perguntas-e-respostas-css');
      wp_register_script('perguntas-e-respostas-js', JS_PATH.'perguntas-e-respostas'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('perguntas-e-respostas-js');
    }
    else{
      wp_register_style('pagina-padrao-css', CSS_PATH.'pagina-padrao.css', null, null, 'all' );
      wp_enqueue_style('pagina-padrao-css');
    }

  }elseif(is_home() || is_category() || is_single() || is_author() || is_search() || is_post_type_archive()){

    if (is_post_type_archive('especialidade-medica') || is_post_type_archive('odontologia') || is_post_type_archive('exames-laboratoriais') || is_post_type_archive('exames-de-imagem') || is_post_type_archive('concursos')) {
      wp_register_style('servicos-archive-css', CSS_PATH.'servicos-archive.css', null, null, 'all' );
      wp_enqueue_style('servicos-archive-css');

      wp_register_script('owl.carousel-js', JS_PATH.'owl.carousel.min'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('owl.carousel-js');
      wp_register_script('servicos-js', JS_PATH.'servicos'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('servicos-js');

    }else if(is_singular('especialidade-medica') || is_singular('odontologia') || is_singular('exames-laboratoriais') || is_singular('exames-de-imagem') || is_singular('concursos')){
      wp_register_style('servicos-single-css', CSS_PATH.'servicos-single.css', null, null, 'all' );
      wp_enqueue_style('servicos-single-css');

      wp_register_script('owl.carousel-js', JS_PATH.'owl.carousel.min'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('owl.carousel-js');
      wp_register_script('perguntas-e-respostas-js', JS_PATH.'perguntas-e-respostas'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('perguntas-e-respostas-js');
      wp_register_script('servicos-single-js', JS_PATH.'servicos-single'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('servicos-single-js');
    }else{
      if(is_single()){
        wp_register_style('single-blog-css', CSS_PATH.'single-blog.css', null, null, 'all' );
        wp_enqueue_style('single-blog-css');

        // DISQUS
        function disqus_embed($disqus_shortname) {
          global $post;
          wp_enqueue_script('disqus_embed','http://'.$disqus_shortname.'.disqus.com/embed.js');
          echo '<div id="disqus_thread"></div>
          <script type="text/javascript">
            var disqus_shortname = "'.$disqus_shortname.'";
            var disqus_title = "'.$post->post_title.'";
            var disqus_url = "'.get_permalink($post->ID).'";
            var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
          </script>';
        }
      }else{
        if(is_search()){
          wp_register_style('search-css', CSS_PATH.'search.css', null, null, 'all' );
          wp_enqueue_style('search-css');
        }else{
          wp_register_style('blog-css', CSS_PATH.'blog.css', null, null, 'all' );
          wp_enqueue_style('blog-css');
        }
      }

      wp_register_script('blog-js', JS_PATH.'blog'.JS_PREFIX.'', null, null, true );
      wp_enqueue_script('blog-js');
    }

  }elseif(is_post_type_archive() || is_singular()){

    if (is_post_type_archive()) {}


  }elseif(is_404()){
    wp_register_style('404-css', CSS_PATH.'404.css', null, null, 'all' );
    wp_enqueue_style('404-css');

  }

  //Chamada de scripts
  wp_enqueue_script('scripts');
  wp_enqueue_script('jquery-ui-js');
  wp_enqueue_script('googlemaps-api');
}


/*Deregister styles and scripts
================================================================================================================*/
function deregister_styles() {

    if (!is_home() && !is_single() && !is_category() && !is_tag() && !is_author()) {
        wp_deregister_style('jetpack-whatsapp');
        wp_deregister_style( 'wp-pagenavi' );
    }

}
add_filter( 'wp_print_styles', 'deregister_styles' );

//Deregister styles and scripts
//================================================================================================================
function deregister_scripts() {

    if (!is_home() && !is_single() && !is_category() && !is_tag() && !is_author()) {
        wp_deregister_script('jetpack-whatsapp');
    }
}
add_filter( 'wp_print_scripts', 'deregister_scripts' );

/*Deregister Contact Form 7 styles
================================================================================================================*/



include(TEMPLATEPATH . '/template-parts/walker.php');

// // Aumenta limite de upload
// @ini_set( 'upload_max_size' , '64M' );
// @ini_set( 'post_max_size', '64M');
// @ini_set( 'max_execution_time', '300' );


//add_filter( 'jetpack_development_mode', '__return_true' );

//ADICIONA SUPORTE A MENUS NO PAINEL ADMIN
add_theme_support('menus');
//ADICIONA SUPORTE POST FORMATS
add_theme_support( 'post-formats', array( 'video' ) );
//REMOVE BARRA DE ADMIN
add_filter('show_admin_bar', '__return_false');
//ENABLE THUMBS
add_theme_support('post-thumbnails');
//UNABLE LOGIN SHOW ERRORS
add_filter('login_errors',create_function('$a', "return null;"));
//REMOVE HEAD VERSION
remove_action('wp_head', 'wp_generator');
//ENABLE THUMBS
add_theme_support( 'post-thumbnails' );


// function tweakjp_rm_comments_att( $open, $post_id ) {
//   $post = get_post( $post_id );
//   if( $post->post_type == 'attachment' ) {
//       return false;
//   }
//   return $open;
// }
// add_filter( 'comments_open', 'tweakjp_rm_comments_att', 10 , 2 );

require_once('wp_bootstrap_navwalker.php');
//require_once('wlaker_dropdown.php');
register_nav_menus(
  array(
    'header'    => __('Header'),
    'footer' => __('Footer')
  )
);


/*=======================================================================================
CUSTOM FUNCTION: LIMIT TEXT
=======================================================================================*/
function the_content_limit($max_char, $conteudo = 'the_content', $more_link_text = '<br/><i class="fa fa-smile-o"></i>', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters($conteudo, $content);
    /* Remove tags do retorno do content */
    $content = strip_tags($content);
    /**/
    $content = str_replace(']]>', ']]>', $content);

   if (strlen($_GET['']) > 0) {
      echo $content;
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
      $content = substr($content, 0, $espacio);
      // $content = $content;

      $conteud = strip_tags($conteudo);
      echo $content;
      echo '...';
   }
   else {
      echo $content;
   }
}

/*=======================================================================================
CUSTOM FUNCTION: LIMIT TEXT TITLE
=======================================================================================*/
function the_title_limit($id, $max_char, $conteudo = 'the_title', $more_link_text = '<br/><i class="fa fa-smile-o"></i>', $stripteaser = 0, $more_file = '') {
    $content = get_the_title($id, $more_link_text);
    $content = apply_filters($conteudo, $content);
    /* Remove tags do retorno do content */
    $content = strip_tags($content);
    /**/
    $content = str_replace(']]>', ']]>', $content);

   if (strlen($_GET['']) > 0) {
      echo $content;
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
      $content = substr($content, 0, $espacio);
      // $content = $content;

      $conteud = strip_tags($conteudo);
      echo $content;
      echo '...';
   }
   else {
      echo $content;
   }
}

/*=======================================================================================
CUSTOM POST TYPE "ODONTOLOGIA"
=======================================================================================*/
add_action('init', 'type_post_odontologia');
function type_post_odontologia() {
  $labels = array(
    'name'                => _x( 'Odontologia', 'Post Type General Name', 'Odontologia' ),
    'singular_name'       => _x( 'Odontologia', 'Post Type Singular Name', 'Odontologia' ),
    'menu_name'           => __( 'Odontologia', 'Odontologia' ),
    'parent_item_colon'   => __( 'Parent Item:', 'Odontologia' ),
    'all_items'           => __( 'Todos os planos odontológicos', 'planos odontológicos' ),
    'view_item'           => __( 'Ver Odontologia', 'Odontologia' ),
    'add_new_item'        => __( 'Add novo Odontologia', 'Odontologia' ),
    'add_new'             => __( 'Add novo', 'Odontologia' ),
    'edit_item'           => __( 'Editar Odontologia', 'Odontologia' ),
    'update_item'         => __( 'Atualizar Odontologia', 'Odontologia' ),
    'search_items'        => __( 'Pesquisar Odontologia', 'Odontologia' ),
    'not_found'           => __( 'Nada encontrado', 'Odontologia' ),
    'not_found_in_trash'  => __( 'Nada encontrado na lixeira', 'Odontologia' ),
    );
  $rewrite = array(
    'slug'                => 'odontologia'
    );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'public_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => $rewrite,
    'capability_type' => 'post',
    'has_archive' => true,
    'menu_icon' => 'dashicons-search',
    'hierarchical' => false,
    'menu_position' => null,

    'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'trackbacks')
    );

    register_post_type( 'odontologia' , $args );
    flush_rewrite_rules();
}


/*=======================================================================================
CUSTOM POST TYPE "EXAMES LABORATORIAIS"
=======================================================================================*/
add_action('init', 'type_post_exames_laboratoriais');
function type_post_exames_laboratoriais() {
  $labels = array(
    'name'                => _x( 'Exames Laboratoriais', 'Post Type General Name', 'Exames Laboratoriais' ),
    'singular_name'       => _x( 'Exames Laboratoriais', 'Post Type Singular Name', 'Exames Laboratoriais' ),
    'menu_name'           => __( 'Exames Laboratoriais', 'Exames Laboratoriais' ),
    'parent_item_colon'   => __( 'Parent Item:', 'Exames Laboratoriais' ),
    'all_items'           => __( 'Todos os exames laboratoriais', 'exames laboratoriais' ),
    'view_item'           => __( 'Ver Exames Laboratoriais', 'Exames Laboratoriais' ),
    'add_new_item'        => __( 'Add novo Exames Laboratoriais', 'Exames Laboratoriais' ),
    'add_new'             => __( 'Add novo', 'Exames Laboratoriais' ),
    'edit_item'           => __( 'Editar Exames Laboratoriais', 'Exames Laboratoriais' ),
    'update_item'         => __( 'Atualizar Exames Laboratoriais', 'Exames Laboratoriais' ),
    'search_items'        => __( 'Pesquisar Exames Laboratoriais', 'Exames Laboratoriais' ),
    'not_found'           => __( 'Nada encontrado', 'Exames Laboratoriais' ),
    'not_found_in_trash'  => __( 'Nada encontrado na lixeira', 'Exames Laboratoriais' ),
    );
  $rewrite = array(
    'slug'                => 'exames-laboratoriais'
    );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'public_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => $rewrite,
    'capability_type' => 'post',
    'has_archive' => true,
    'menu_icon' => 'dashicons-welcome-write-blog',
    'hierarchical' => false,
    'menu_position' => null,

    'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'trackbacks')
    );

    register_post_type( 'exames-laboratoriais' , $args );
    flush_rewrite_rules();
}

/*=======================================================================================
CUSTOM POST TYPE "EXAMES DE IMAGEM"
=======================================================================================*/
add_action('init', 'type_post_exames_de_imagem');
function type_post_exames_de_imagem() {
  $labels = array(
    'name'                => _x( 'Exames de Imagem', 'Post Type General Name', 'Exames de Imagem' ),
    'singular_name'       => _x( 'Exames de Imagem', 'Post Type Singular Name', 'Exames de Imagem' ),
    'menu_name'           => __( 'Exames de Imagem', 'Exames de Imagem' ),
    'parent_item_colon'   => __( 'Parent Item:', 'Exames de Imagem' ),
    'all_items'           => __( 'Todos os exames de imagem', 'exames de imagem' ),
    'view_item'           => __( 'Ver Exames de Imagem', 'Exames de Imagem' ),
    'add_new_item'        => __( 'Add novo Exames de Imagem', 'Exames de Imagem' ),
    'add_new'             => __( 'Add novo', 'Exames de Imagem' ),
    'edit_item'           => __( 'Editar Exames de Imagem', 'Exames de Imagem' ),
    'update_item'         => __( 'Atualizar Exames de Imagem', 'Exames de Imagem' ),
    'search_items'        => __( 'Pesquisar Exames de Imagem', 'Exames de Imagem' ),
    'not_found'           => __( 'Nada encontrado', 'Exames de Imagem' ),
    'not_found_in_trash'  => __( 'Nada encontrado na lixeira', 'Exames de Imagem' ),
    );
  $rewrite = array(
    'slug'                => 'exames-de-imagem'
    );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'public_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => $rewrite,
    'capability_type' => 'post',
    'has_archive' => true,
    'menu_icon' => 'dashicons-format-image',
    'hierarchical' => false,
    'menu_position' => null,

    'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'trackbacks')
    );

    register_post_type( 'exames-de-imagem' , $args );
    flush_rewrite_rules();
}


/*=======================================================================================
CUSTOM POST TYPE "ESPECIALIDADE MÉDICA"
=======================================================================================*/
add_action('init', 'type_post_especialidade_medica');
function type_post_especialidade_medica() {
  $labels = array(
    'name'                => _x( 'Especialidades Médicas', 'Post Type General Name', 'especialidades médicas' ),
    'singular_name'       => _x( 'Especialidade Médica', 'Post Type Singular Name', 'especialidade médica' ),
    'menu_name'           => __( 'Especialidades Médicas', 'especialidade médica' ),
    'parent_item_colon'   => __( 'Parent Item:', 'Especialidade Médica' ),
    'all_items'           => __( 'Todas as especialidades médicas', 'especialidades médicas' ),
    'view_item'           => __( 'Ver Especialidade Médica', 'especialidade médica' ),
    'add_new_item'        => __( 'Add novo Especialidade Médica', 'especialidade médica' ),
    'add_new'             => __( 'Add novo', 'Especialidade Médica' ),
    'edit_item'           => __( 'Editar Especialidade Médica', 'especialidade médica' ),
    'update_item'         => __( 'Atualizar Especialidade Médica', 'especialidade médica' ),
    'search_items'        => __( 'Pesquisar Especialidade Médica', 'especialidade médica' ),
    'not_found'           => __( 'Nada encontrado', 'especialidade médica' ),
    'not_found_in_trash'  => __( 'Nada encontrado na lixeira', 'especialidade médica' ),
    );
  $rewrite = array(
    'slug'                => 'especialidade-medica'
    );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'public_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => $rewrite,
    'capability_type' => 'post',
    'has_archive' => true,
    'menu_icon' => 'dashicons-plus-alt',
    'hierarchical' => false,
    'menu_position' => null,

    'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'trackbacks')
    );

    register_post_type( 'especialidade-medica' , $args );
    flush_rewrite_rules();
}
/*=======================================================================================
CUSTOM POST TYPE "CONVÊNIOS"
=======================================================================================*/
add_action('init', 'type_post_concursos');
function type_post_concursos() {
  $labels = array(
    'name'                => _x( 'Concursos', 'Post Type General Name', 'Concursos' ),
    'singular_name'       => _x( 'Concursos', 'Post Type Singular Name', 'Concursos' ),
    'menu_name'           => __( 'Concursos', 'Concursos' ),
    'parent_item_colon'   => __( 'Parent Item:', 'Concursos' ),
    'all_items'           => __( 'Todas os Concursos', 'Concursos' ),
    'view_item'           => __( 'Ver Concursos', 'Concursos' ),
    'add_new_item'        => __( 'Add novo Concursos', 'Concursos' ),
    'add_new'             => __( 'Add novo', 'Concursos' ),
    'edit_item'           => __( 'Editar Concursos', 'Concursos' ),
    'update_item'         => __( 'Atualizar Concursos', 'Concursos' ),
    'search_items'        => __( 'Pesquisar Concursos', 'Concursos' ),
    'not_found'           => __( 'Nada encontrado', 'Concursos' ),
    'not_found_in_trash'  => __( 'Nada encontrado na lixeira', 'Concursos' ),
    );
  $rewrite = array(
    'slug'                => 'concursos'
    );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'public_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => $rewrite,
    'capability_type' => 'post',
    'has_archive' => true,
    'menu_icon' => 'dashicons-welcome-learn-more',
    'hierarchical' => false,
    'menu_position' => null,

    'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'trackbacks')
    );

    register_post_type( 'concursos' , $args );
    flush_rewrite_rules();
}

/*=======================================================================================
CUSTOM POST TYPE "CONVÊNIOS"
=======================================================================================*/
add_action('init', 'type_post_convenio');
function type_post_convenio() {
  $labels = array(
    'name'                => _x( 'Convênio', 'Post Type General Name', 'convênio' ),
    'singular_name'       => _x( 'Convênio', 'Post Type Singular Name', 'Convênio' ),
    'menu_name'           => __( 'Convênio', 'Convênio' ),
    'parent_item_colon'   => __( 'Parent Item:', 'Convênio' ),
    'all_items'           => __( 'Todas os convênio', 'convênio' ),
    'view_item'           => __( 'Ver Convênio', 'Convênio' ),
    'add_new_item'        => __( 'Add novo Convênio', 'Convênio' ),
    'add_new'             => __( 'Add novo', 'Convênio' ),
    'edit_item'           => __( 'Editar Convênio', 'Convênio' ),
    'update_item'         => __( 'Atualizar Convênio', 'Convênio' ),
    'search_items'        => __( 'Pesquisar Convênio', 'Convênio' ),
    'not_found'           => __( 'Nada encontrado', 'Convênio' ),
    'not_found_in_trash'  => __( 'Nada encontrado na lixeira', 'Convênio' ),
    );
  $rewrite = array(
    'slug'                => 'convenio'
    );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'public_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => $rewrite,
    'capability_type' => 'post',
    'has_archive' => true,
    'menu_icon' => 'dashicons-id-alt',
    'hierarchical' => false,
    'menu_position' => null,

    'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'trackbacks')
    );

    register_post_type( 'convenio' , $args );
    flush_rewrite_rules();
}

  function more_post_ajax(){
      $competencia = $_POST['competencia'];
      $customFildConvenio = '';

      // Preenchimento com o slug do Custom Post Type e também
      // Preenchimento Custom Field referente ao Convênio.
      if($competencia == 'Odontologia'){
        $competencia = 'odontologia';
        $customFildConvenio = 'convenio_odontologia';

      }elseif($competencia == 'Especialidades Médicas'){
        $competencia = 'especialidade-medica';
        $customFildConvenio = 'convenio_especialidade_medica';

      }elseif($competencia == 'Exames de Imagem'){
        $competencia = 'exames-de-imagem';
        $customFildConvenio = 'convenio_exames_de_imagem';

      }elseif($competencia == 'Exames Laboratoriais'){
        $competencia = 'exames-laboratoriais';
        $customFildConvenio = 'convenio_exames_laboratoriais';

      }elseif($competencia == 'Concursos'){
        $competencia = 'concursos';
        $customFildConvenio = 'convenio_concursos';
      }


      header("Content-Type: text/html");

      $args = array(
          'post_type'       => $competencia,
          'posts_per_page'  => -1
      );

      $loop = new WP_Query($args);

      echo '<option value="">Selecione um Serviço</option>';
      while ($loop->have_posts()) { $loop->the_post();
        echo '<option value="'.get_the_title().'">'.get_the_title().'</option>';
      }
      wp_reset_query();
      echo ' divisor ';

      $argsServPage = array(
          'post__in' => array( 419 ),
          'post_type' => 'page'
      );
      $queryServPage = new WP_Query( $argsServPage );

      $queryServPage->the_post();
      echo '<option value="">Selecione um Convênio</option>';

      $convenio = get_field($customFildConvenio);
      foreach( $convenio as $p):
        echo '<option value="'.get_the_title($p->ID).'">'.get_the_title($p->ID).'</option>';
      endforeach;
      wp_reset_query();
  }

  add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
  add_action('wp_ajax_more_post_ajax', 'more_post_ajax');


  function get_convenios_ajax(){
    $competencia = $_POST['competencia'];
    $customFildConvenio = '';
    header("Content-Type: text/html");

    // Preenchimento com o slug do Custom Post Type e também
    // Preenchimento Custom Field referente ao Convênio.
    if($competencia == 'Odontologia'){
      $customFildConvenio = 'convenio_odontologia';

    }elseif($competencia == 'Especialidades Médicas'){
      $customFildConvenio = 'convenio_especialidade_medica';

    }elseif($competencia == 'Exames de Imagem'){
      $customFildConvenio = 'convenio_exames_de_imagem';

    }elseif($competencia == 'Exames Laboratoriais'){
      $customFildConvenio = 'convenio_exames_laboratoriais';

    }elseif($competencia == 'Concursos'){
        $customFildConvenio = 'convenio_concursos';
      }

    $argsServPage = array(
        'post__in' => array( 419 ),
        'post_type' => 'page'
    );
    $queryServPage = new WP_Query( $argsServPage );

    $queryServPage->the_post();
    $convenio = get_field($customFildConvenio);


    foreach( $convenio as $p):
      $titleCodif = html_entity_decode(get_the_title($p->ID));
      echo $titleCodif.'|';
    endforeach;
    wp_reset_query();

  }
  add_action('wp_ajax_nopriv_get_convenios_ajax', 'get_convenios_ajax');
  add_action('wp_ajax_get_convenios_ajax', 'get_convenios_ajax');


/*ADICIONA FAVICON À TELA DE LOGIN E AO PAINEL DO SITE*/
add_action( 'login_head', 'favicon_admin' );
add_action( 'admin_head', 'favicon_admin' );
add_filter( 'wpcf7_support_html5', '__return_false' );
function favicon_admin() {
    $favicon_url = THEMEURL . '/assets/img/favicon';
    $favicon  = '<!-- Favicon IE 9 -->';
    $favicon .= '<!--[if lte IE 9]><link rel="icon" type="image/x-icon" href="' . $favicon_url . '.ico" /> <![endif]-->';
    $favicon .= '<!-- Favicon Outros Navegadores -->';
    $favicon .= '<link rel="shortcut icon" type="image/png" href="' . $favicon_url . '.png" />';
    $favicon .='<!-- Favicon iPhone -->';
    $favicon .='<link rel="apple-touch-icon" href="' . $favicon_url . '.png" />';
    echo $favicon;
}

function login_function() {
    add_filter( 'gettext', 'username_change', 20, 3 );
    function username_change( $translated_text, $text, $domain )
    {
        if ($text === 'Username or Email Address')
        {
            $translated_text = 'Endereço de Email';
        }
        return $translated_text;
    }
}
add_action( 'login_head', 'login_function' );


// Insert into your functions.php and have fun creating login error msgs
function guwp_error_msgs() {
    // insert how many msgs you want as an array item. it will be shown randomly
    $custom_error_msgs = array(
        'Ocorreu algum erro, entre em contato com o administrador.'
    );
    // get random array item to show
    return $custom_error_msgs[array_rand($custom_error_msgs)];;
}
add_filter( 'login_errors', 'guwp_error_msgs' );

function wpbeginner_remove_version_asso() {
  return '';
}
add_filter('the_generator', 'wpbeginner_remove_version_asso');


/*=======================================================================================
STYLE FOR LOGIN PAGE
=======================================================================================*/
//Link na tela de login para a página inicial
// Custom WordPress Login Logo
function my_login_logo() {
  echo '
    <style type="text/css">
      html{
        background-color: #000;
      }
      .login #login_error{
        display: none;
      }
      body.login div#login h1 a {
        pointer-events: none;
        background-image: url(wp-content/themes/lapecco/assets/img/lapecco-nome.svg);
        padding-bottom: 0px;
        background-size: contain;
        width: 270px;
      }
      body.login {
        background-image: url(wp-content/themes/lapecco/assets/img/bg-header-default-large.jpg);
        background-size: cover;
        background-repeat: no-repeat;
        background-position: 50%;
      }
      .login form {
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
        background:#FFF;
        opacity:0.90;
        border-radius:5px;
      }
      .login #backtoblog a, .login #nav a {
        text-decoration: none;
        color: #FFF !important;
        font-weight:bold;
      }
      #backtoblog{
      margin-top:0 !important;
    }
      .login label {
        color: #0072bc !important;
        font-size: 14px;
      }
      .login input{
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -ms-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
      }
      .wp-core-ui .button.button-large {
        height: 51px !important;
        line-height: 28px;
        margin-top:15px;
        color: #FFF;
        border-color: transparent !important;
        padding: 0px 12px 2px;
        width: 100%;
        background: #0072bc none repeat scroll 0% 0% !important;
        border-radius: 0px;
        text-transform: uppercase;
      }
      .wp-core-ui .button.button-large:hover{
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
      }
      .login input[type=text]:focus,
      .login input[type=password]:focus{
        -webkit-box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
        -moz-box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
        box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
      }
    </style>
  ';
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );




// Wordpress pagination
function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /**
   * We construct the pagination arguments to enter into our paginate_links
   * function.
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      //echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }

}



/*MAIS LIDAS
======================*/
// Verifica se não existe nenhuma função com o nome post_count_session_start
if ( ! function_exists( 'post_count_session_start' ) ) {
    // Cria a função
    function post_count_session_start() {
        // Inicia uma sessão PHP
        if ( ! session_id() ) session_start();
    }
    // Executa a ação
    add_action( 'init', 'post_count_session_start' );
}

// Verifica se não existe nenhuma função com o nome tp_count_post_views
if ( ! function_exists( 'tp_count_post_views' ) ) {
    // Conta os views do post
    function tp_count_post_views () {
        // Garante que vamos tratar apenas de posts
        if ( is_single() ) {

            // Precisamos da variável $post global para obter o ID do post
            global $post;

            // Se a sessão daquele posts não estiver vazia
            if ( empty( $_SESSION[ 'tp_post_counter_' . $post->ID ] ) ) {

                // Cria a sessão do posts
                $_SESSION[ 'tp_post_counter_' . $post->ID ] = true;

                // Cria ou obtém o valor da chave para contarmos
                $key = 'tp_post_counter';
                $key_value = get_post_meta( $post->ID, $key, true );

                // Se a chave estiver vazia, valor será 1
                if ( empty( $key_value ) ) { // Verifica o valor
                    $key_value = 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } else {
                    // Caso contrário, o valor atual + 1
                    $key_value += 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } // Verifica o valor

            } // Checa a sessão

        } // is_single
        return;
      }
    add_action( 'get_header', 'tp_count_post_views' );
}
//FUNÇÃO PARA SUPORTE A UPLOAD DE IMAGENS SVG
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/*
ENABLE SVG UPLOAD
==================*/
add_filter( 'wp_check_filetype_and_ext', function($filetype_ext_data, $file, $filename, $mimes) {
  if ( substr($filename, -4) === '.svg' ) {
    $filetype_ext_data['ext'] = 'svg';
    $filetype_ext_data['type'] = 'image/svg+xml';
  }
  return $filetype_ext_data;
}, 100, 4 );



