<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#0072BC"><!-- Define a cor da janela para android -->
		<title><?php the_title();?></title>

		<!--Favicon-->
		<link rel="shortcut icon" href="<?php echo THEMEURL; ?>/assets/img/favicon.png" >
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-152x152.png">

		<?php
			include(TEMPLATEPATH . '/template-parts/loop-style.php');
			wp_head();
		?>


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if IE ]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<link rel="stylesheet" type="text/css" href="ie.css" />
		<![endif]-->

		<!-- Light Box curtir facebook -->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=360193087652668";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	</head>
	<body <?php body_class(); ?>>
		<div id="wrap" class="cd-main-content">
			<header class="header" id="header-sroll">
				<div class="my-container">
					<div class="row">
						<div class="col-xs-12">
							<div class="desk-menu">
								<div class="logo">
									<?php
										if(is_front_page()): ?>
											<h1 class="logo-adn">
												<a class="siteLogo" data-g-label="Grupo de Saúde Lapecco" title="Grupo de Saúde Lapecco" href="<?php echo SITEURL; ?>">Grupo de Saúde Lapecco</a>
											</h1>
										<?php else: ?>
											<span class="logo-adn">
												<a class="siteLogo" data-g-label="Grupo de Saúde Lapecco" title="Grupo de Saúde Lapecco" href="<?php echo SITEURL; ?>">Grupo de Saúde Lapecco</a>
											</span>
										<?php endif;
									?>
								</div>
								<nav class="box-menu">
									<?php
								      	wp_nav_menu(
								      		array(
										        'menu'            => 'Header',
										 		'depth'           => 2,
										        'container_id'    => '',
										        'menu_class'      => 'cd-primary-nav',
										        'menu_id'         => 'cd-primary-nav',
										        'echo'            => true,
										        'before'          => '',
										        'after'           => '',
										        'link_before'     => '',
										        'link_after'      => '',
										        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										        'walker'        => new themeslug_walker_nav_menu
								      		)
								      	);
							      	?>
							      	<div class="bx-resul-mob">
							      		<a href="<?php echo SITEURL; ?>/resultados-online/" class="menu-link">Resultados online</a>
							      	</div>
									<div class="cd-header-buttons">
										<a class="cd-nav-trigger" href="#cd-primary-nav">Menu<span></span></a>
									</div>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div class="conteudo">