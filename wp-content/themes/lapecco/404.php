<?php get_header(); ?>

<section class="head-title" style="background-image:url('<?php echo THEMEURL ?>/assets/img/bg-header-default-single.jpg')">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-name">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs" class="stay">','</p>');
                    } ?>
                    <h1 class="title-princ">Página 404</h1>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="the-error">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="search404">
					<h2>Infelizmente, não encontramos essa página.</h2>
					<div class="error">
						<p>Talvez o menu acima tenha o que você procura!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>